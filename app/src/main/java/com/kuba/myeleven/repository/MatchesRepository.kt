package com.kuba.myeleven.repository

import androidx.lifecycle.MutableLiveData
import com.kuba.myeleven.data.dao.EventDao
import com.kuba.myeleven.data.dao.MatchLineUpsDao
import com.kuba.myeleven.data.entities.MatchLineUp
import com.kuba.myeleven.data.entities.PlayerMatchStats
import com.kuba.myeleven.data.entities.composite.TeamNewMatchModel
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class MatchesRepository @Inject constructor(
        private val matchLineUpsDao: MatchLineUpsDao,
        private val eventsDao: EventDao,
        private val teamsRepository: TeamsRepository,
        private val playersRepository: PlayersRepository) {

    fun getMatchesForTeam(teamId: Int): Observable<List<MatchLineUp>> {
        return Observable.just(matchLineUpsDao.getTeamLineUps(teamId))
    }

    fun getPlayersForTeam(teamId: Int): Observable<TeamNewMatchModel> {
        return Observable.zip(
                teamsRepository.getTeam(teamId),
                playersRepository.getPlayers(teamId),
                BiFunction { team, players ->
                    TeamNewMatchModel(team, players)
                })
    }

    fun saveMatchLineup(matchLineUp: MatchLineUp): Observable<Int> {
        return Observable.just(matchLineUpsDao.insert(matchLineUp))
                .map { matchLineUpsDao.getLastLineupId(matchLineUp.editTime) }
    }

    fun savePlayers(playersStats: MutableList<PlayerMatchStats>): Observable<Unit> {
       return Observable.just(matchLineUpsDao.insertAll(playersStats))
    }
}