package com.kuba.myeleven.repository

import com.kuba.myeleven.data.dao.PlayersDao
import com.kuba.myeleven.data.entities.Player
import io.reactivex.Observable
import javax.inject.Inject

class PlayersRepository @Inject constructor(private val playersDao: PlayersDao) {

    fun getPlayers(teamId: Int): Observable<List<Player>> {
        return Observable.just(playersDao.getPlayersByTeamId(teamId))
    }

    fun getPlayer(playerId: Int): Observable<Player> {
        return Observable.just(playersDao.getPlayerDetails(playerId))
    }

    fun deletePlayer(playerId: Int): Observable<Unit> {
        return getPlayer(playerId).map { playersDao.delete(it) }
    }

    fun savePlayer(player: Player): Observable<Unit> {
        return Observable.just(playersDao.insert(player))
    }

    fun updatePlayer(player: Player): Observable<Unit> {
        return Observable.just(playersDao.update(player))
    }
}