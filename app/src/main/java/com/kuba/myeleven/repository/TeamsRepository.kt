package com.kuba.myeleven.repository

import com.kuba.myeleven.data.dao.TeamsDao
import com.kuba.myeleven.data.entities.Team
import io.reactivex.Observable
import javax.inject.Inject

class TeamsRepository @Inject constructor(private val teamsDao: TeamsDao) {

    fun saveTeam(team: Team): Observable<Unit> {
        return Observable.just(teamsDao.insert(team))
    }

    fun updateTeam(team: Team): Observable<Unit> {
        return Observable.just(teamsDao.update(team))
    }

    fun getAllTeams(): Observable<List<Team>> {
        return Observable.just(teamsDao.getTeams())
    }

    fun getTeam(teamId: Int): Observable<Team> {
        return Observable.just(teamsDao.getTeamById(teamId))
    }

    fun deleteTeam(teamId: Int): Observable<Unit> {
        return getTeam(teamId).map { it -> teamsDao.delete(it) }
    }
}