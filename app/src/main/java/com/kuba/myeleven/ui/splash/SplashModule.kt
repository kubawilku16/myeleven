package com.kuba.myeleven.ui.splash

import androidx.fragment.app.FragmentActivity
import dagger.Binds
import dagger.Module

@Module
abstract class SplashModule {

    @Binds
    abstract fun activity(splashActivity: SplashActivity): FragmentActivity
}