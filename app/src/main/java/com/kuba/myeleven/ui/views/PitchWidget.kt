package com.kuba.myeleven.ui.views

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.kuba.myeleven.R
import com.kuba.myeleven.data.entities.composite.PlayerFullDetails
import com.kuba.myeleven.data.enums.Jersey
import com.kuba.myeleven.data.enums.LineUp
import com.kuba.myeleven.data.enums.Position
import com.kuba.myeleven.ext.gone
import com.kuba.myeleven.ext.visible
import icepick.Icepick
import icepick.State
import kotlinx.android.synthetic.main.widget_pitch.view.*

class PitchWidget : FrameLayout {

    @State
    lateinit var lineUp: LineUp

    @State
    lateinit var jersey: Jersey

    var playerClickListener: OnPlayerClickListener? = null

    constructor(context: Context) : super(context) {
        View.inflate(context, R.layout.widget_pitch, this)
    }

    constructor(context: Context, attributesSet: AttributeSet) : super(context, attributesSet) {
        View.inflate(context, R.layout.widget_pitch, this)
    }

    constructor(context: Context, attributesSet: AttributeSet, defStyleAttr: Int) : super(context, attributesSet, defStyleAttr) {
        View.inflate(context, R.layout.widget_pitch, this)
    }

    fun setLineup(lineUp: LineUp, players: List<PlayerFullDetails>? = null) {
        this.lineUp = lineUp
        resetLineUp()
        var positionCounter = 1
        goalKeeper.visible()
        bindPlayer(goalKeeper, Position.GOALKEEPER, positionCounter++, 0, players)
        bindLine(defendersLineup, lineUp.def, Position.DEFENDER, positionCounter, players)
        positionCounter += lineUp.def
        bindLine(midfieldersMainLineup, lineUp.mid, Position.MIDFIELDER, positionCounter, players)
        positionCounter += lineUp.mid
        if (lineUp.extraMid > 0) {
            midfieldersExtraLineup.visible()
            bindLine(midfieldersExtraLineup, lineUp.extraMid, Position.MIDFIELDER, positionCounter, players)
            positionCounter += lineUp.extraMid
        } else {
            midfieldersExtraLineup.gone()
        }
        bindLine(forwardsLineup, lineUp.str, Position.STRIKER, positionCounter, players)
    }

    private fun bindLine(linearLayout: LinearLayout, repeats: Int, position: Position, startingPosition: Int, players: List<PlayerFullDetails>?) {
        repeat(repeats) { it ->
            linearLayout.addView(PlayerWidget(context).apply {
                bindPlayer(this, position, startingPosition + it, it, players)
            })
        }
    }

    private fun bindPlayer(playerWidget: PlayerWidget, position: Position, positionNumber: Int,
                           positionInLine: Int,
                           players: List<PlayerFullDetails>?) {
        playerWidget.apply {
            startAnimation(AnimationUtils.loadAnimation(context, R.anim.alpha_with_slide))
            if (position != Position.GOALKEEPER) {
                layoutParams = LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f).apply {
                    when (position) {
                        Position.GOALKEEPER -> {
                        }
                        Position.DEFENDER -> {
                            if (lineUp.def > 4 && (positionInLine == 0 || positionInLine == lineUp.def - 1)) {
                                bottomMargin = context.resources.getDimension(R.dimen.double_spacer).toInt()
                            }
                        }
                        Position.MIDFIELDER -> {
                        }
                        Position.STRIKER -> {
                            if (lineUp.str > 2 && (positionInLine == 0 || positionInLine == lineUp.str - 1)) {
                                topMargin = context.resources.getDimension(R.dimen.double_spacer).toInt()
                            }
                        }
                    }
                }
            }
            setAndBindJersey(this@PitchWidget.jersey)
            this.position = position
            this.positionNumber = positionNumber
            setPlayer(players?.firstOrNull { player -> player.stats.position == this.positionNumber }?.player)
            setOnPitchListener(playerClickListener)
        }
    }

    private fun resetLineUp() {
        goalKeeper.gone()
        defendersLineup.removeAllViews()
        midfieldersExtraLineup.removeAllViews()
        midfieldersMainLineup.removeAllViews()
        forwardsLineup.removeAllViews()
    }

    override fun onSaveInstanceState(): Parcelable? {
        return Icepick.saveInstanceState(this, super.onSaveInstanceState())
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        super.onRestoreInstanceState(Icepick.restoreInstanceState(this, state))
    }

    fun changeJersey(jersey: Jersey) {
        this.jersey = jersey
        goalKeeper.setAndBindJersey(jersey)
        changePlayersJerseys(defendersLineup)
        changePlayersJerseys(midfieldersExtraLineup)
        changePlayersJerseys(midfieldersMainLineup)
        changePlayersJerseys(forwardsLineup)

    }

    private fun changePlayersJerseys(parent: LinearLayout) {
        repeat(parent.childCount) {
            if (parent.getChildAt(it) is PlayerWidget) {
                (parent.getChildAt(it) as PlayerWidget).setAndBindJersey(jersey)
            }
        }
    }

    interface OnPlayerClickListener {
        fun onClick(position: Position, positionNumber: Int, playerWidget: PlayerWidget)
    }
}