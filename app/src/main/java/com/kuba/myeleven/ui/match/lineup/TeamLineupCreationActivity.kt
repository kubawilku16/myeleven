package com.kuba.myeleven.ui.match.lineup

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.Observer
import com.f2prateek.dart.InjectExtra
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter
import com.kuba.myeleven.R
import com.kuba.myeleven.data.entities.Player
import com.kuba.myeleven.data.entities.Team
import com.kuba.myeleven.data.enums.LineUp
import com.kuba.myeleven.data.enums.Position
import com.kuba.myeleven.databinding.ActivityLineupCreationBinding
import com.kuba.myeleven.ext.toast
import com.kuba.myeleven.ext.visible
import com.kuba.myeleven.ui.base.BaseActivity
import com.kuba.myeleven.ui.match.selector.PlayerSelectorDialog
import com.kuba.myeleven.ui.match.selector.PlayerSelectorDialogBuilder
import com.kuba.myeleven.ui.views.BenchWidget
import com.kuba.myeleven.ui.views.PitchWidget
import com.kuba.myeleven.ui.views.PlayerWidget
import com.kuba.myeleven.ui.views.shared.SpinnerAdapter
import com.kuba.myeleven.utils.OperationError
import com.kuba.myeleven.utils.OperationSuccess
import com.kuba.myeleven.utils.TimeUtils
import kotlinx.android.synthetic.main.activity_lineup_creation.*
import kotlinx.android.synthetic.main.widget_player.view.*
import java.util.*

class TeamLineupCreationActivity : BaseActivity<TeamLineupCreationViewModel, ActivityLineupCreationBinding>(TeamLineupCreationViewModel::class.java) {
    override val layoutId: Int
        get() = R.layout.activity_lineup_creation
    override val screenTitle: String
        get() = ""


    @InjectExtra
    @Nullable
    @JvmField
    var teamId: Int? = null

    private val lineups = LineUp.values().toList()
    private lateinit var adapter: SpinnerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model.team.observe(this, Observer { initTeamData(it) })
        model.jersey.observe(this, Observer {
            teamLineupPitch.changeJersey(it)
            teamLineupBench.changeJersey(it)
        })
        model.saveStatus.observe(this, Observer {
            when (it) {
                is OperationSuccess -> {
                    navigator.finish(result = RESULT_OK, intent = intent)
                }
                is OperationError -> {
                    applicationContext.toast("OperationError: ${it.throwable.message}")
                }
            }
        })
        model.loadDataForTeam(teamId!!)
    }

    private fun initListeners() {
        teamLineupFirstJersey.playerRoot.setOnClickListener {
            if (teamLineupFirstJersey.jersey != model.jersey) {
                model.jersey.value = teamLineupFirstJersey.jersey
            }
        }
        teamLineupSecondJersey.playerRoot.setOnClickListener {
            if (teamLineupSecondJersey.jersey != model.jersey) {
                model.jersey.value = teamLineupSecondJersey.jersey
            }
        }
        teamLineupThirdJersey.playerRoot.setOnClickListener {
            if (teamLineupThirdJersey.jersey != model.jersey) {
                model.jersey.value = teamLineupThirdJersey.jersey
            }
        }
        teamLineupPitch.playerClickListener = object : PitchWidget.OnPlayerClickListener {
            override fun onClick(position: Position, positionNumber: Int, playerWidget: PlayerWidget) {
                onPlayerClick(positionNumber, playerWidget)
            }
        }
        teamLineupBench.listener = object : BenchWidget.OnPlayerClickListener {
            override fun onClick(positionNumber: Int, widget: PlayerWidget) {
                onPlayerClick(positionNumber, widget)
            }
        }
        teamLineupExpandLayout.setOnClickListener { onExpandSquadClick() }
        teamLineupPitchLayout.setListener(object : ExpandableLayoutListenerAdapter() {
            override fun onPreOpen() {
                teamLineupExpandArrow.rotation = 180f
            }

            override fun onPreClose() {
                teamLineupExpandArrow.rotation = 0f
            }
        })
        teamLineupBenchExpandLayout.setOnClickListener { onExpandBenchClick() }
        teamLineupBenchLayout.setListener(object : ExpandableLayoutListenerAdapter() {
            override fun onPreOpen() {
                teamLineupBenchExpandArrow.rotation = 180f
            }

            override fun onPreClose() {
                teamLineupBenchExpandArrow.rotation = 0f
            }
        })
        teamLineupCreationSaveBtn.setOnClickListener {
            try {
                validate()
                model.save()
            } catch (e: Exception) {
                showNativeMessage(title = getString(R.string.common_error_occured),
                        message = e.message!!,
                        confirmButtonText = getString(R.string.common_ok),
                        dialogCancelable = true,
                        drawableResId = R.drawable.ic_soccer_error)
            }
        }
        teamLineupCreationDate.setOnEditorActionListener { _, _, _ ->
            if (!teamLineupCreationDate.text.toString().matches(Regex("([0-9]{2})-([0-9]{2})-([0-9]{4}) ([0-9]{2}):([0-9]{2})"))) {
                teamLineupCreationDate.setText("")
                applicationContext.toast(R.string.error_incorrect_date_format)
            }
            false
        }
        teamLineupCreationCalendar.setOnClickListener {
            var date = ""
            val cal = Calendar.getInstance()
            if (model.eventDate.value != null) {
                cal.time = model.eventDate.value
            }
            val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, yy, mm, dd ->
                date = "$dd-${mm + 1}-$yy"
                TimePickerDialog(this@TeamLineupCreationActivity, TimePickerDialog.OnTimeSetListener { _, hh, mm ->
                    date += " $hh:$mm"
                    model.eventDate.value = TimeUtils.convertToDate(date)
                    teamLineupCreationDate.setText(date, TextView.BufferType.EDITABLE)
                }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
            dialog.datePicker.maxDate = System.currentTimeMillis()
            dialog.show()
        }
    }

    private fun onPlayerClick(positionNumber: Int, playerWidget: PlayerWidget) {
        if (model.players.value?.isNotEmpty() == true) {
            val dialog = PlayerSelectorDialogBuilder(playerWidget.name.isNotEmpty()).build()
            dialog.initData(model.getEnabledPlayersForPosition(),
                    object : PlayerSelectorDialog.PlayerSelectorInterface {
                        override fun onDelete() {
                            model.removePlayerAtPosition(positionNumber)
                            playerWidget.setPlayer(null)
                        }

                        override fun onSelect(player: Player) {
                            model.addPlayerAtPosition(player, positionNumber)
                            playerWidget.setPlayer(player)
                        }
                    }, teamLineupPitch.jersey)
            showDialogFragment(PlayerSelectorDialog.TAG, dialog)
        } else {
            showNativeMessage(title = getString(R.string.common_error_occured),
                    message = getString(R.string.error_no_players),
                    dialogCancelable = true,
                    drawableResId = R.drawable.ic_soccer_error)
        }
    }

    private fun validate() {
        if (!model.firstSquadIsSet()) {
            throw Exception(getString(R.string.error_first_squad_not_full))
        }
    }

    private fun onExpandBenchClick() {
        if (teamLineupBenchLayout.isExpanded) {
            teamLineupBenchLayout.toggle()
        } else {
            teamLineupBenchLayout.expand()
        }
    }

    private fun onExpandSquadClick() {
        if (teamLineupPitchLayout.isExpanded) {
            teamLineupPitchLayout.toggle()
        } else {
            teamLineupPitchLayout.expand()
        }
    }

    private fun initSpinner() {
        adapter = object : SpinnerAdapter(this, R.layout.list_item_spinner_number, lineups.map { it.getLineUpText() }) {
            override fun enabled(item: Any) = true
        }
        teamLineupSpinner.adapter = adapter
        teamLineupSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                teamLineupPitch.setLineup(lineups[p2], model.getPlayerFullDetailsList())
            }
        }
    }

    private fun initTeamData(team: Team) {
        model.jersey.value = team.firstJersey
        teamLineupFirstJersey.setAndBindJersey(team.firstJersey)
        teamLineupSecondJersey.setAndBindJersey(team.secondJersey)
        if (team.thirdJersey != null) {
            teamLineupThirdJersey.setAndBindJersey(team.thirdJersey!!)
            teamLineupThirdJersey.visible()
        }
        teamLineupJerseys.visible()
        initListeners()
        initSpinner()
        teamLineupBench.init()
    }
}