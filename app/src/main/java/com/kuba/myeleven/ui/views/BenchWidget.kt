package com.kuba.myeleven.ui.views

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.kuba.myeleven.R
import com.kuba.myeleven.data.enums.Jersey
import icepick.Icepick
import icepick.State
import kotlinx.android.synthetic.main.widget_bench.view.*

class BenchWidget : FrameLayout {

    @State
    lateinit var jersey: Jersey

    var listener: OnPlayerClickListener? = null

    constructor(context: Context) : super(context) {
        View.inflate(context, R.layout.widget_bench, this)
    }

    constructor(context: Context, attributesSet: AttributeSet) : super(context, attributesSet) {
        View.inflate(context, R.layout.widget_bench, this)
    }

    constructor(context: Context, attributesSet: AttributeSet, defStyleAttr: Int) : super(context, attributesSet, defStyleAttr) {
        View.inflate(context, R.layout.widget_bench, this)
    }

    fun init() {
        bindLine(benchFirstLine, 12, 4)
        bindLine(benchSecondLine, 16, 3)
        changePlayersJersey(benchFirstLine)
        changePlayersJersey(benchSecondLine)
    }

    private fun bindLine(linearLayout: LinearLayout, startingPosition: Int, count: Int) {
        linearLayout.removeAllViews()
        repeat(count) {
            linearLayout.addView(PlayerWidget(context).apply {
                layoutParams = LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f)
                startAnimation(AnimationUtils.loadAnimation(context, R.anim.alpha_with_slide))
                this.positionNumber = startingPosition + it
                setOnBenchListener(listener)
            })
        }
    }

    fun changeJersey(jersey: Jersey) {
        this.jersey = jersey
        changePlayersJersey(benchFirstLine)
        changePlayersJersey(benchSecondLine)
    }

    private fun changePlayersJersey(linearLayout: LinearLayout) {
        repeat(linearLayout.childCount) {
            if (linearLayout.getChildAt(it) is PlayerWidget) {
                (linearLayout.getChildAt(it) as PlayerWidget).setAndBindJersey(jersey)
            }
        }
    }

    override fun onSaveInstanceState(): Parcelable? {
        return Icepick.saveInstanceState(this, super.onSaveInstanceState())
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        super.onRestoreInstanceState(Icepick.restoreInstanceState(this, state))
    }

    interface OnPlayerClickListener {
        fun onClick(positionNumber: Int, widget: PlayerWidget)
    }
}