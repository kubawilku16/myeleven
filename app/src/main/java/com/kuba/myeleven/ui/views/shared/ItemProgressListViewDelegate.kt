package com.kuba.myeleven.ui.views.shared

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kuba.myeleven.R
import com.kuba.myeleven.ui.views.list.BaseAdapterDelegate
import kotlinx.android.extensions.LayoutContainer

class ItemProgressListViewDelegate : BaseAdapterDelegate<ListProgress, ItemProgressListViewDelegate.UserProgressBlockViewHolder>(ListProgress::class.java) {

    override val layoutId: Int
        get() = R.layout.list_progress_item

    override fun bindViewHolder(item: ListProgress, holder: UserProgressBlockViewHolder, position: Int, itemsSize: Int) {
        holder.bindTo(item)
    }

    override fun createViewHolder(view: View): UserProgressBlockViewHolder {
        return UserProgressBlockViewHolder(view)
    }

    inner class UserProgressBlockViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bindTo(item: ListProgress) {
            with(item) {
            }
        }
    }
}