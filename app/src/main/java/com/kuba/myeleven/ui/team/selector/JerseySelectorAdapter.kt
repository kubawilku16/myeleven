package com.kuba.myeleven.ui.team.selector

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kuba.myeleven.R
import com.kuba.myeleven.data.enums.Jersey
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_jersey.*

class JerseySelectorAdapter(private val items: List<Jersey>) : RecyclerView.Adapter<JerseySelectorAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context!!).inflate(R.layout.list_item_jersey, parent, false))
    }

    override fun getItemCount() = items.count()


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(position: Int) {
            with(items[position]) {
                listJerseyPlayer.setAndBindJersey(this)
            }
        }
    }
}