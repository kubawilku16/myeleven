package com.kuba.myeleven.ui.match.selector

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import com.kuba.myeleven.R
import com.kuba.myeleven.data.entities.Player
import com.kuba.myeleven.data.enums.Jersey
import com.kuba.myeleven.ext.dpToPx
import com.kuba.myeleven.ext.gone
import com.kuba.myeleven.ui.base.dialogs.BaseDialog
import com.kuba.myeleven.utils.CustomBundler
import com.kuba.myeleven.utils.SliderLayoutManager
import icepick.State
import kotlinx.android.synthetic.main.dialog_player_selector.*

@FragmentWithArgs
class PlayerSelectorDialog : BaseDialog() {

    @Arg
    @JvmField
    var canBeDeleted = true

    @State(CustomBundler::class)
    lateinit var players: MutableList<Player>

    @State(CustomBundler::class)
    lateinit var jersey: Jersey

    @State
    @JvmField
    var position = 0

    private var listener: PlayerSelectorInterface? = null
    private lateinit var adapter: PlayerSelectorAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_player_selector, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindCustomView()
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        super.onViewCreated(view, savedInstanceState)
    }

    private fun bindCustomView() {
        if (!canBeDeleted) {
            dialogSelectorDeleteBtn.gone()
        }
        adapter = PlayerSelectorAdapter(players, jersey)
        val layoutManager = SliderLayoutManager(context!!, dialogSelectorList)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        layoutManager.callback = object : SliderLayoutManager.OnItemSelectedListener {
            override fun onItemSelected(layoutPosition: Int) {
                position = layoutPosition
            }
        }
        dialogSelectorList.layoutManager = layoutManager
        dialogSelectorList.setHasFixedSize(true)
        adapter.notifyDataSetChanged()
        dialogSelectorList.adapter = adapter
        adapter.notifyDataSetChanged()
        dialogSelectorList.smoothScrollToPosition(position)
        val dm = DisplayMetrics()
        (context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getMetrics(dm)
        val horizontalPadding = dm.widthPixels / 2 + dialog.window!!.decorView.width.dpToPx(16)
        val verticalPadding = resources.getDimension(R.dimen.single_spacer).toInt()
        dialogSelectorList.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding)
        LinearSnapHelper().attachToRecyclerView(dialogSelectorList)
        dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        dialogSelectorAcceptBtn.setOnClickListener {
            listener?.onSelect(players[position])
            dialog.dismiss()
        }
        dialogSelectorDeleteBtn.setOnClickListener {
            listener?.onDelete()
            dialog.dismiss()
        }
        dialogSelectorCancelBtn.setOnClickListener { dialog.dismiss() }
    }

    fun initData(players: List<Player>, listener: PlayerSelectorInterface, jersey: Jersey) {
        this.players = players.toMutableList()
        this.listener = listener
        this.jersey = jersey
    }

    interface PlayerSelectorInterface {
        fun onDelete()
        fun onSelect(player: Player)
    }

    companion object {
        const val TAG = "PlayerSelectorDialog"
    }
}