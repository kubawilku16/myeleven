package com.kuba.myeleven.ui.team.manage.matches

import com.kuba.myeleven.data.entities.MatchLineUp
import com.kuba.myeleven.repository.MatchesRepository
import com.kuba.myeleven.ui.base.BaseViewModel
import com.kuba.myeleven.ui.base.Factory
import io.reactivex.Observable
import javax.inject.Inject

class TeamMatchesViewModel : BaseViewModel() {

    @Inject
    lateinit var matchesRepository: MatchesRepository

    var teamId = -1

    var view: TeamMatchesView? = null

    override fun init() {
        initRequest(GET_LINEUPS_REQUEST_ID, object : Factory<Observable<List<MatchLineUp>>>{
            override fun create(): Observable<List<MatchLineUp>> {
                return matchesRepository.getMatchesForTeam(teamId)
                        .subscribeOn(mainExecutionThread.scheduler())
                        .observeOn(postExecutionThread.scheduler)
            }
        },
                {
                    stop(GET_LINEUPS_REQUEST_ID)
                    view?.onLineupsLoadSuccess(it)
                },
                {
                    stop(GET_LINEUPS_REQUEST_ID)
                    view?.onLineupsLoadError(it)
                })
    }

    fun loadLineups(teamId: Int) {
        this.teamId = teamId
        start(GET_LINEUPS_REQUEST_ID)
    }

    interface TeamMatchesView {
        fun onLineupsLoadSuccess(result: List<MatchLineUp>)
        fun onLineupsLoadError(throwable: Throwable)
    }

    companion object {
        private const val GET_LINEUPS_REQUEST_ID = 5000
    }
}