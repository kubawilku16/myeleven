package com.kuba.myeleven.ui.match.lineup

import androidx.fragment.app.FragmentActivity
import dagger.Binds
import dagger.Module

@Module
abstract class TeamLineupCreationModule {

    @Binds
    abstract fun activity(teamLineupCreationActivity: TeamLineupCreationActivity): FragmentActivity
}