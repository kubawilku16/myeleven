package com.kuba.myeleven.ui.home.teams

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class TeamsModule {

    @Binds
    abstract fun fragment(teamsFragment: TeamsFragment): Fragment
}