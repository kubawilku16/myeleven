package com.kuba.myeleven.ui.views.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Base delegate implementation, providing easier creation of lists containing many different
 * types of views.
 *
 * @param <T>
 * @param <S>
 */

abstract class BaseAdapterDelegate<in T, S : RecyclerView.ViewHolder>(private val itemClass: Class<T>) {

    var viewType: Int = 0

    protected abstract val layoutId: Int

    fun isForViewType(items: List<*>, position: Int): Boolean {
        return itemClass.isInstance(items[position])
    }

    fun onCreateViewHolder(parent: ViewGroup): S {
        return createViewHolder(LayoutInflater.from(parent.context).inflate(layoutId, parent, false))
    }

    @Suppress("UNCHECKED_CAST")
    fun onBindViewHolder(item: T, holder: RecyclerView.ViewHolder, position: Int, itemsSize: Int) {
        bindViewHolder(item, holder as S, position, itemsSize)
    }

    protected open fun onViewRecycled(holder: S) {}

    protected abstract fun createViewHolder(view: View): S

    protected abstract fun bindViewHolder(item: T, holder: S, position: Int, itemsSize: Int)

    open fun getItemId(item: T): Long {
        return -1
    }
}