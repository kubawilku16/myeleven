package com.kuba.myeleven.ui.base

interface Factory<T> {
    fun create(): T
}