package com.kuba.myeleven.ui.team.selector

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import com.kuba.myeleven.R
import com.kuba.myeleven.data.enums.Jersey
import com.kuba.myeleven.ext.dpToPx
import com.kuba.myeleven.ui.base.dialogs.BaseDialog
import com.kuba.myeleven.utils.CustomBundler
import com.kuba.myeleven.utils.SliderLayoutManager
import icepick.State
import kotlinx.android.synthetic.main.dialog_jersey_selector.*

@FragmentWithArgs
class JerseySelectorDialog : BaseDialog() {

    @Arg
    @JvmField
    var title = ""

    @State(CustomBundler::class)
    @JvmField
    var position = 0

    private val jerseys = Jersey.values().toMutableList()
    private var listener: JerseySelectorListener? = null
    private lateinit var adapter: JerseySelectorAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_jersey_selector, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindCustomView()
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        super.onViewCreated(view, savedInstanceState)
    }

    private fun bindCustomView() {
        jerseySelectorTitle.text = title
        adapter = JerseySelectorAdapter(Jersey.values().toList())
        val layoutManager = SliderLayoutManager(context!!, jerseySelectorList)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        layoutManager.callback = object : SliderLayoutManager.OnItemSelectedListener {
            override fun onItemSelected(layoutPosition: Int) {
                position = layoutPosition
            }
        }
        jerseySelectorList.layoutManager = layoutManager
        jerseySelectorList.setHasFixedSize(true)
        adapter.notifyDataSetChanged()
        jerseySelectorList.adapter = adapter
        adapter.notifyDataSetChanged()
        jerseySelectorList.smoothScrollToPosition(position)
        val dm = DisplayMetrics()
        (context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getMetrics(dm)
        val horizontalPadding = dm.widthPixels / 2 + dialog.window!!.decorView.width.dpToPx(16)
        val verticalPadding = resources.getDimension(R.dimen.single_spacer).toInt()
        jerseySelectorList.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding)
        LinearSnapHelper().attachToRecyclerView(jerseySelectorList)
        dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        jerseySelectorChooseBtn.setOnClickListener {
            listener?.onSelect(jerseys[position])
            dialog.dismiss()
        }
        jerseySelectorCancelBtn.setOnClickListener { dialog.dismiss() }
    }

    fun setListener(listener: JerseySelectorListener) {
        this.listener = listener
    }

    interface JerseySelectorListener {
        fun onSelect(jersey: Jersey)
    }

    companion object {
        const val TAG = "JerseySelectorDialog"
    }
}