package com.kuba.myeleven.ui.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.hannesdorfmann.fragmentargs.FragmentArgs
import com.kuba.myeleven.R
import com.kuba.myeleven.ext.invisible
import com.kuba.myeleven.ext.visible
import com.kuba.myeleven.injection.BaseInjector
import com.kuba.myeleven.ui.base.dialogs.NativeDialog
import com.kuba.myeleven.ui.base.dialogs.NativeDialogBuilder
import com.kuba.myeleven.ui.base.dialogs.ProgressDialog
import com.kuba.myeleven.ui.base.dialogs.ProgressDialogBuilder
import com.kuba.myeleven.ui.base.navigation.FragmentNavigator
import com.kuba.myeleven.utils.CustomBundler
import com.kuba.myeleven.utils.KeyboardUtils
import dagger.android.support.AndroidSupportInjection
import icepick.Icepick
import icepick.State
import kotlinx.android.synthetic.main.custom_toolbar.*
import javax.inject.Inject


@SuppressLint("ValidFragment")
abstract class BaseFragment<ViewModel : BaseViewModel, Binding : ViewDataBinding>(private var clazz: Class<ViewModel>) : Fragment() {

    @Inject
    lateinit var navigator: FragmentNavigator

    @Inject
    lateinit var dialogStateManager: DialogStateManager

    @State(CustomBundler::class)
    @JvmField
    var disposablesIds = mutableListOf<Int>()

    @State
    @JvmField
    var isUiLocked = false

    protected lateinit var model: ViewModel

    protected lateinit var binding: Binding

    protected abstract val layoutId: Int

    protected abstract val screenTitle: String

    protected open val isDisplayingBackArrow: Boolean
        get() = true

    protected open val isDisplayingShareIcon: Boolean
        get() = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FragmentArgs.inject(this)
        Icepick.restoreInstanceState(this, savedInstanceState)
    }

    @SuppressLint("ResourceType")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater, layoutId, container, false)
        binding.setLifecycleOwner(this)
        model = ViewModelProviders.of(this).get(clazz)
        (activity!!.applicationContext as BaseInjector).inject(model)
        model.init()
        model.isUiLocked.value = isUiLocked
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        initView()
    }

    abstract fun initView()

    private fun setupToolbar() {
        if (toolbar != null) {
            toolbarTitle.run {
                if (screenTitle.isNotEmpty()) {
                    text = screenTitle
                }
            }
            toolbarBackButton.run {
                if (isDisplayingBackArrow) {
                    visible()
                    setOnClickListener {
                        KeyboardUtils.hideKeyboard(this@BaseFragment)
                        activity?.onBackPressed()
                    }
                } else
                    invisible()
            }
            toolbarShareBtn.run {
                if (isDisplayingShareIcon) {
                    visible()
                    setOnClickListener { onShareClick() }
                } else
                    invisible()
            }
        }
    }

    protected fun onShareClick() {

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Icepick.saveInstanceState(this, outState)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    /**
     * Shows given DialogFragment
     *
     * @param tag
     * @param fragment
     */
    protected fun showDialogFragment(tag: String, fragment: DialogFragment) {
        try {
            val ft = fragmentManager?.beginTransaction()
            val oldFragment = fragmentManager?.findFragmentByTag(tag)
            oldFragment?.run {
                ft?.remove(this)
            }
            fragment.show(ft, tag)
            fragmentManager?.executePendingTransactions()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Shows dialog with progress indicator
     *
     * @param title
     * @param message
     */
    fun showProgressDialog(title: String? = null, message: String = getString(R.string.action_please_wait), cancelable: Boolean = false) {
        hideProgressDialog()
        val builder = ProgressDialogBuilder(cancelable, message)
        title?.let { builder.title(it) }
        showDialogFragment(ProgressDialog.TAG, builder.build())
    }

    /**
     * Hides dialog with progress indicator
     */
    fun hideProgressDialog() {
        try {
            val fragment = fragmentManager?.findFragmentByTag(ProgressDialog.TAG)
            fragment?.let {
                (it as DialogFragment).dismiss()
            }
        } catch (e: Exception) {
        }
    }

    fun showNativeMessage(
            title: String = getString(R.string.info_attention),
            message: String,
            requestCode: Int = 0,
            confirmButtonText: String? = null,
            declineButtonText: String? = null,
            dialogListener: NativeDialog.NativeDialogListener? = null,
            dialogCancelable: Boolean = true,
            timeForDialog: Long? = null,
            taskAfterDismiss: Runnable? = null,
            drawableResId: Int? = null
    ) {
        val dialogBuilder = NativeDialogBuilder(dialogCancelable, message, requestCode, title)
        if (drawableResId != null) {
            dialogBuilder.drawableRes(drawableResId)
        }
        if (confirmButtonText != null) {
            dialogBuilder.positiveText(confirmButtonText)
        }
        if (declineButtonText != null) {
            dialogBuilder.negativeText(declineButtonText)
        }
        val dialog = dialogBuilder.build()
        if (dialogListener != null) {
            dialog.setDialogListener(dialogListener)
        }
        dialog.isCancelable = dialogCancelable
        showDialogFragment(NativeDialog.TAG, dialog)
        if (timeForDialog != null && timeForDialog > 0) {
            setTimeForDialogAndExecuteTask(dialog, timeForDialog, taskAfterDismiss)
        }
    }

    private fun setTimeForDialogAndExecuteTask(fragment: DialogFragment, timeToCloseDialog: Long, task: Runnable?) {
        val runnable = Runnable {
            fragment.dialog?.dismiss()
            task?.run()
        }
        fragment.dialog?.setOnCancelListener { dialogStateManager.removeRunnable(runnable) }
        dialogStateManager.addRunnable(runnable, timeToCloseDialog)
    }
}