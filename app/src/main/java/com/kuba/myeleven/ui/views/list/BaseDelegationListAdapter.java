package com.kuba.myeleven.ui.views.list;

import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import timber.log.Timber;

/**
 * Base adapter for creating lists with delegates.
 */
public class BaseDelegationListAdapter extends RecyclerView.Adapter {

    protected List<BaseAdapterDelegate> delegates = new ArrayList<>();
    private List items;
    private int nextViewType = 0;

    /**
     * Adds items to list
     *
     * @param items
     */
    @SuppressWarnings("unchecked")
    public void setItems(List items) {
        if (this.items == null) {
            this.items = new ArrayList();
        }
        int startPosition = this.items.size() + 1;
        this.items.addAll(items);
        int endPosition = this.items.size();
        if (startPosition == 1) {
            notifyDataSetChanged();
        } else {
            notifyItemRangeInserted(startPosition, endPosition);
        }
    }

    /**
     * Remove item from list at specified position
     *
     * @param position
     */
    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, items.size());
    }

    /**
     * Refresh items in adapter
     *
     * @param items
     */
    public void setReloadedList(List items) {
        if (this.items == null)
            this.items = new ArrayList();
        else
            this.items.clear();

        setItems(items);
    }

    @SuppressWarnings("unchecked")
    public List<Object> getItems() {
        if (items == null) {
            items = new ArrayList();
        }
        return items;
    }

    /**
     * Add another delegate to list of available delegates
     *
     * @param delegate
     */
    public void addDelegate(BaseAdapterDelegate delegate) {
        if (nextViewType == Integer.MAX_VALUE) {
            nextViewType = 0;
        }

        delegate.setViewType(nextViewType++);
        delegates.add(delegate);
    }

    public List<BaseAdapterDelegate> getDelegates() {
        return delegates;
    }

    /**
     * Remove delegates on specified index in list
     *
     * @param index
     */
    public void removeDelegate(int index) {
        delegates.remove(index);
    }

    /**
     * Clear delegates list
     */
    public void removeDelegates() {
        delegates.clear();
    }

    @Override
    public int getItemViewType(int position) {
        for (int i = 0; i < delegates.size(); i++) {
            if (delegates.get(i).isForViewType(items, position)) {
                return delegates.get(i).getViewType();
            }
        }

        return -1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        for (int i = 0; i < delegates.size(); i++) {
            if (delegates.get(i).getViewType() == viewType) {
                return delegates.get(i).onCreateViewHolder(parent);
            }
        }

        // Print error info.
        StringBuilder logViewTypes = new StringBuilder();

        for (int i = 0; i < delegates.size(); i++) {
            logViewTypes.append(delegates.get(i).getViewType()).append("; ");
        }

        Timber.e("Required view type: " + viewType + "\nAvailable delegate view types (" + getClass().getSimpleName() + "):\n" + logViewTypes);
        throw new IllegalArgumentException("No delegate found.");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();

        for (int i = 0; i < delegates.size(); i++) {
            if (delegates.get(i).getViewType() == viewType) {
                delegates.get(i).onBindViewHolder(items.get(position), holder, position, items.size());
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        int viewType = holder.getItemViewType();

        for (int i = 0; i < delegates.size(); i++) {
            if (delegates.get(i).getViewType() == viewType) {
                delegates.get(i).onViewRecycled(holder);
            }
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setDiffItems(@NotNull DiffUtil.Callback callback, List items) {
        this.items = new ArrayList();
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(callback, false);
        this.items.addAll(items);
        diffResult.dispatchUpdatesTo(this);
    }
}
