package com.kuba.myeleven.ui.player.details

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class PlayerDetailsModule {

    @Binds
    abstract fun fragment(playerDetailsFragment: PlayerDetailsFragment): Fragment
}