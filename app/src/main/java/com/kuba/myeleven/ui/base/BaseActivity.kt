package com.kuba.myeleven.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.f2prateek.dart.Dart
import com.kuba.myeleven.R
import com.kuba.myeleven.ext.gone
import com.kuba.myeleven.ext.visible
import com.kuba.myeleven.injection.BaseInjector
import com.kuba.myeleven.ui.base.dialogs.NativeDialog
import com.kuba.myeleven.ui.base.dialogs.NativeDialogBuilder
import com.kuba.myeleven.ui.base.dialogs.ProgressDialog
import com.kuba.myeleven.ui.base.dialogs.ProgressDialogBuilder
import com.kuba.myeleven.ui.base.navigation.Navigator
import com.kuba.myeleven.utils.CustomBundler
import com.kuba.myeleven.utils.KeyboardUtils
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import icepick.Icepick
import icepick.State
import kotlinx.android.synthetic.main.custom_toolbar.*
import javax.inject.Inject

abstract class BaseActivity<ViewModel : BaseViewModel, Binding : ViewDataBinding>(private var clazz: Class<ViewModel>) :
        AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var dialogStateManager: DialogStateManager

    @Inject
    protected lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

    @State(CustomBundler::class)
    @JvmField
    var disposablesIds = mutableListOf<Int>()

    @State
    @JvmField
    var isUiLocked = false

    protected lateinit var model: ViewModel

    protected lateinit var binding: Binding

    protected abstract val layoutId: Int

    protected abstract val screenTitle: String

    protected open val isDisplayingBackArrow: Boolean
        get() = true

    protected open val isDisplayingShareIcon: Boolean
        get() = false

    protected open val isDisplayingDeleteIcon: Boolean
        get() = false

    protected open val isDisplayingEditIcon: Boolean
        get() = false

    override fun onCreate(savedInstanceState: Bundle?) {
        Dart.inject(this)
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        Icepick.restoreInstanceState(this, savedInstanceState)
        setContentView(layoutId)
        model = ViewModelProviders.of(this).get(clazz)
        (application as BaseInjector).inject(model)
        model.init()
        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.setLifecycleOwner(this)
        model.isUiLocked.value = isUiLocked
        setupToolbar()
    }

    override fun onResume() {
        super.onResume()
        model.resume(disposablesIds)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        disposablesIds = model.getDisposablesIdsToSave()
        isUiLocked = model.isUiLocked.value ?: false
        Icepick.saveInstanceState(this, outState)
    }

    private fun setupToolbar() {
        if (toolbar != null) {
            toolbarTitle.run {
                if (screenTitle.isNotEmpty()) {
                    text = screenTitle
                }
            }
            toolbarBackButton.run {
                if (isDisplayingBackArrow) {
                        visible()
                    setOnClickListener {
                        KeyboardUtils.hideKeyboard(this)
                        onBackPressed()
                    }
                } else
                        gone()
            }
            toolbarShareBtn.run {
                if (isDisplayingShareIcon) {
                        expand()
                    setOnClickListener { onShareClick() }
                } else
                        toggle()
            }
            toolbarEditBtn.run {
                if (isDisplayingShareIcon) {
                        expand()
                    setOnClickListener { onEditClick() }
                } else
                        toggle()
            }
            toolbarDeleteBtn.run {
                if (isDisplayingShareIcon) {
                        expand()
                    setOnClickListener { onDeleteClick() }
                } else
                        toggle()
            }
        }
    }

    protected fun onShareClick() {}

    protected fun onDeleteClick() {}

    protected fun onEditClick() {}

    /**
     * Shows given DialogFragment
     *
     * @param tag
     * @param fragment
     */
    protected fun showDialogFragment(tag: String, fragment: DialogFragment) {
        try {
            val ft = supportFragmentManager.beginTransaction()
            val oldFragment = supportFragmentManager.findFragmentByTag(tag)
            oldFragment?.run {
                ft.remove(this)
            }
            fragment.show(ft, tag)

            supportFragmentManager.executePendingTransactions()
        } catch (e: Exception) {
        }
    }

    /**
     * Shows dialog with progress indicator
     *
     * @param title
     * @param message
     */
    fun showProgressDialog(title: String? = null, message: String = getString(R.string.action_please_wait), cancelable: Boolean = false) {
        hideProgressDialog()
        val builder = ProgressDialogBuilder(cancelable, message)
        title?.let { builder.title(it) }
        showDialogFragment(ProgressDialog.TAG, builder.build())
    }

    /**
     * Hides dialog with progress indicator
     */
    fun hideProgressDialog() {
        try {
            val fragment = supportFragmentManager.findFragmentByTag(ProgressDialog.TAG)
            fragment?.let {
                (it as DialogFragment).dismiss()
            }
        } catch (e: Exception) {
        }
    }

    fun showNativeMessage(
            title: String = getString(R.string.info_attention),
            message: String,
            requestCode: Int = 0,
            confirmButtonText: String? = null,
            declineButtonText: String? = null,
            dialogListener: NativeDialog.NativeDialogListener? = null,
            dialogCancelable: Boolean = true,
            timeForDialog: Long? = null,
            taskAfterDismiss: Runnable? = null,
            drawableResId: Int? = null
    ) {
        val dialogBuilder = NativeDialogBuilder(dialogCancelable, message, requestCode, title)
        if (drawableResId != null) {
            dialogBuilder.drawableRes(drawableResId)
        }
        if (confirmButtonText != null) {
            dialogBuilder.positiveText(confirmButtonText)
        }
        if (declineButtonText != null) {
            dialogBuilder.negativeText(declineButtonText)
        }
        val dialog = dialogBuilder.build()
        if (dialogListener != null) {
            dialog.setDialogListener(dialogListener)
        }
        dialog.isCancelable = dialogCancelable
        showDialogFragment(NativeDialog.TAG, dialog)
        if (timeForDialog != null && timeForDialog > 0) {
            setTimeForDialogAndExecuteTask(dialog, timeForDialog, taskAfterDismiss)
        }
    }

    private fun setTimeForDialogAndExecuteTask(fragment: DialogFragment, timeToCloseDialog: Long, task: Runnable?) {
        val runnable = Runnable {
            fragment.dialog?.dismiss()
            task?.run()
        }
        fragment.dialog?.setOnCancelListener { dialogStateManager.removeRunnable(runnable) }
        dialogStateManager.addRunnable(runnable, timeToCloseDialog)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return supportFragmentInjector
    }
}