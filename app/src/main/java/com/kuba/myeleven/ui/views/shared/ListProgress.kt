package com.kuba.myeleven.ui.views.shared

import org.parceler.Parcel
import org.parceler.ParcelConstructor

@Parcel(Parcel.Serialization.BEAN)
class ListProgress @ParcelConstructor constructor()