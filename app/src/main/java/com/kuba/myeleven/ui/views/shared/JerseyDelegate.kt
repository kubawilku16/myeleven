package com.kuba.myeleven.ui.views.shared

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kuba.myeleven.R
import com.kuba.myeleven.data.enums.Jersey
import com.kuba.myeleven.ui.views.list.BaseAdapterDelegate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_jersey.*

class JerseyDelegate : BaseAdapterDelegate<Jersey, JerseyDelegate.ViewHolder>(Jersey::class.java) {
    override val layoutId: Int
        get() = R.layout.list_item_jersey

    override fun createViewHolder(view: View): ViewHolder = ViewHolder(view)

    override fun bindViewHolder(item: Jersey, holder: ViewHolder, position: Int, itemsSize: Int) {
        holder.bind(item, position)
    }

    inner class ViewHolder(override var containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(item: Jersey, position: Int) {
            with(item) {
                listJerseyPlayer.setAndBindJersey(this)
            }
        }
    }
}