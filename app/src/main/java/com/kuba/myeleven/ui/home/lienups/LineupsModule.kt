package com.kuba.myeleven.ui.home.lienups

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class LineupsModule {

    @Binds
    abstract fun fragment(lineupsFragment: LineupsFragment): Fragment
}