package com.kuba.myeleven.ui.base.navigation

import android.app.Activity.RESULT_OK
import android.content.Intent
import androidx.fragment.app.FragmentActivity
import com.kuba.myeleven.Henson
import com.kuba.myeleven.ui.team.creation.TeamCreationActivity
import com.kuba.myeleven.utils.KeyboardUtils
import javax.inject.Inject

open class Navigator @Inject constructor(private val activity: FragmentActivity) {

    protected open fun startActivityForResult(intent: Intent, requestCode: Int) {
        activity.startActivityForResult(intent, requestCode)
    }

    protected open fun startActivity(intent: Intent) {
        activity.startActivity(intent)
    }

    fun goToHomeActivity(showWelcomeDialog: Boolean) {
        startActivity(Henson.with(activity).gotoHomeActivity().showWelcomeDialog(showWelcomeDialog).build())
    }

    fun goToTeamCreationActivity(id: Int? = null) {
        if (id != null) {
            startActivityForResult(Henson.with(activity).gotoTeamCreationActivity().teamId(id).build(), SAVE_TEAM_SUCCESS_REQUEST_CODE)
        } else {
            startActivityForResult(Intent(activity, TeamCreationActivity::class.java), SAVE_TEAM_SUCCESS_REQUEST_CODE)
        }
    }

    fun goToPlayerCreationActivity(playerId: Int?, teamId: Int?) {
        if (playerId != null && teamId != null) {
            startActivityForResult(Henson.with(activity).gotoPlayerCreationActivity().playerId(playerId).teamId(teamId).build(), SAVE_PLAYER_SUCCESS_REQUEST_CODE)
        } else if (playerId == null && teamId != null) {
            startActivityForResult(Henson.with(activity).gotoPlayerCreationActivity().teamId(teamId).build(), SAVE_PLAYER_SUCCESS_REQUEST_CODE)
        } else if (playerId != null && teamId == null) {
            startActivityForResult(Henson.with(activity).gotoPlayerCreationActivity().playerId(playerId).build(), SAVE_PLAYER_SUCCESS_REQUEST_CODE)
        } else {
            startActivityForResult(Henson.with(activity).gotoPlayerCreationActivity().build(), SAVE_PLAYER_SUCCESS_REQUEST_CODE)
        }
    }

    fun goToManageTeamActivity(teamId: Int, teamName: String, color: Int) {
        startActivity(Henson.with(activity).gotoTeamManageActivity().mainColor(color).teamId(teamId).teamName(teamName).build())
    }

    fun goToTeamLineupCreationActivity(teamId: Int) {
        startActivity(Henson.with(activity).gotoTeamLineupCreationActivity().teamId(teamId).build())
    }

    fun finish(result: Int = RESULT_OK, intent: Intent? = null) {
        KeyboardUtils.hideKeyboard(activity)
        activity.setResult(result, intent)
        activity.finish()
    }

    companion object {
        const val SAVE_TEAM_SUCCESS_REQUEST_CODE = 10000
        const val SAVE_PLAYER_SUCCESS_REQUEST_CODE = 10001
    }
}