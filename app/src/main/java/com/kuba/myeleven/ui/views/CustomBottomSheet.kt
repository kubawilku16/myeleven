package com.kuba.myeleven.ui.views

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.kuba.myeleven.R
import com.kuba.myeleven.ext.gone
import icepick.Icepick
import icepick.State
import kotlinx.android.synthetic.main.widget_custom_bottom_sheet.view.*

class CustomBottomSheet : FrameLayout {

    var clickListener: OnClickListener? = null

    constructor(context: Context) : super(context) {
        initView()
    }

    constructor(context: Context, attributesSet: AttributeSet) : super(context, attributesSet) {
        initView()
    }

    constructor(context: Context, attributesSet: AttributeSet, defStyleAttr: Int) : super(context, attributesSet, defStyleAttr) {
        initView()
    }


    fun setBottomSheetTitle(text: String) {
        bottomSheetTitle.text = text
    }

    private fun initView() {
        View.inflate(context, R.layout.widget_custom_bottom_sheet, this)
        bottomSheetDeleteBtn.setOnClickListener { clickListener?.onDeleteClick() }
        bottomSheetEditBtn.setOnClickListener { clickListener?.onEditClick() }
    }

    override fun onSaveInstanceState(): Parcelable? {
        return Icepick.saveInstanceState(this, super.onSaveInstanceState())
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        super.onRestoreInstanceState(Icepick.restoreInstanceState(this, state))
    }

    interface OnClickListener {
        fun onDeleteClick()
        fun onEditClick()
    }
}