package com.kuba.myeleven.ui.base.dialogs

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.text.Html
import androidx.fragment.app.DialogFragment
import com.afollestad.materialdialogs.MaterialDialog
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs

@FragmentWithArgs
class ProgressDialog : BaseDialog() {

    @Arg(required = false)
    var title: String? = null
    @Arg
    var message: String? = null
    @Arg
    var dialogCancelable: Boolean = false

    private var listener: ProgressDialogListener? = null

    interface ProgressDialogListener {
        fun onProgressDialogCancelled(dialog: DialogFragment)
    }

    override fun onAttach(activity: Context?) {
        super.onAttach(activity)
        if (activity is ProgressDialogListener)
            listener = activity
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = dialogCancelable
    }

    @Suppress("DEPRECATION")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = MaterialDialog.Builder(activity!!)
        builder.run {
            title?.let {
                title(it)
            }
            progress(true, 0)
            cancelable(dialogCancelable)

            message?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    content(Html.fromHtml(it, Html.FROM_HTML_MODE_LEGACY))
                } else {
                    content(Html.fromHtml(it))
                }
            }
        }

        return builder.show()
    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
        listener?.run {
            onProgressDialogCancelled(this@ProgressDialog)
        }
    }

    companion object {
        const val TAG = "progress_dialog"
    }
}