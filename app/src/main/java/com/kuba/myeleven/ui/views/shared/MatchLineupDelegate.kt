package com.kuba.myeleven.ui.views.shared

import android.annotation.SuppressLint
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kuba.myeleven.R
import com.kuba.myeleven.data.entities.MatchLineUp
import com.kuba.myeleven.ui.views.list.BaseAdapterDelegate
import com.kuba.myeleven.utils.TimeUtils
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_lineup.*

class MatchLineupDelegate(private val listener: OnClickListener) : BaseAdapterDelegate<MatchLineUp, MatchLineupDelegate.ViewHolder>(MatchLineUp::class.java) {
    override val layoutId: Int
        get() = R.layout.list_item_lineup

    override fun createViewHolder(view: View): ViewHolder = ViewHolder(view)

    override fun bindViewHolder(item: MatchLineUp, holder: ViewHolder, position: Int, itemsSize: Int) {
        holder.bind(item)
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        @SuppressLint("SetTextI18n")
        fun bind(item: MatchLineUp) {
            with(item) {
                containerView.setOnClickListener { listener.onClick(matchId) }
                when {
                    opponentName?.isNotEmpty() == true -> {
                        listLineupText.text = opponentName
                    }
                    time != null -> {
                        listLineupText.text = TimeUtils.convertTimeToString(time)
                    }
                    else -> {
                        listLineupText.text = "[${TimeUtils.convertTimeToString(editTime)}]${lineup.getLineUpText()}"
                    }
                }
            }
        }
    }

    interface OnClickListener {
        fun onClick(lineupId: Int)
    }
}