package com.kuba.myeleven.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import com.kuba.myeleven.R
import kotlinx.android.synthetic.main.widget_empty_list_placeholder.view.*

class EmptyListPlaceholder : FrameLayout {
    constructor(context: Context) : super(context) {
        View.inflate(context, R.layout.widget_empty_list_placeholder, this)
        runAnimation()
    }

    constructor(context: Context, attributesSet: AttributeSet) : super(context, attributesSet) {
        View.inflate(context, R.layout.widget_empty_list_placeholder, this)
        runAnimation()
    }

    constructor(context: Context, attributesSet: AttributeSet, defStyleAttr: Int) : super(context, attributesSet, defStyleAttr) {
        View.inflate(context, R.layout.widget_empty_list_placeholder, this)
        runAnimation()
    }

    private fun runAnimation() {
        emptyListTapImage.animation = AnimationUtils.loadAnimation(context, R.anim.alpha_infinite)
        emptyListTapImage.animate()
    }
}