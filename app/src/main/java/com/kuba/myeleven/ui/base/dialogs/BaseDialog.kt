package com.kuba.myeleven.ui.base.dialogs

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.hannesdorfmann.fragmentargs.FragmentArgs
import icepick.Icepick

open class BaseDialog : DialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FragmentArgs.inject(this)
        Icepick.restoreInstanceState(this, savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Icepick.saveInstanceState(this, outState)
    }
}