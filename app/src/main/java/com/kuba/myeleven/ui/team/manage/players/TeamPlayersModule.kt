package com.kuba.myeleven.ui.team.manage.players

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class TeamPlayersModule {

    @Binds
    abstract fun fragment(teamPlayersFragment: TeamPlayersFragment): Fragment
}