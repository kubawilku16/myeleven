package com.kuba.myeleven.ui.match.lineup

import androidx.lifecycle.MutableLiveData
import com.kuba.myeleven.data.entities.MatchLineUp
import com.kuba.myeleven.data.entities.Player
import com.kuba.myeleven.data.entities.PlayerMatchStats
import com.kuba.myeleven.data.entities.Team
import com.kuba.myeleven.data.entities.composite.PlayerFullDetails
import com.kuba.myeleven.data.entities.composite.TeamNewMatchModel
import com.kuba.myeleven.data.enums.Jersey
import com.kuba.myeleven.data.enums.LineUp
import com.kuba.myeleven.data.enums.Position
import com.kuba.myeleven.repository.MatchesRepository
import com.kuba.myeleven.ui.base.BaseViewModel
import com.kuba.myeleven.ui.base.Factory
import com.kuba.myeleven.utils.OperationError
import com.kuba.myeleven.utils.OperationStatus
import com.kuba.myeleven.utils.OperationSuccess
import com.kuba.myeleven.utils.TimeUtils
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class TeamLineupCreationViewModel : BaseViewModel() {
    val players = MutableLiveData<MutableList<Player>>()
    val team = MutableLiveData<Team>()
    val jersey = MutableLiveData<Jersey>()
    val lineup = MutableLiveData<LineUp>()
    val eventDate = MutableLiveData<Date>()
    val opponentName = MutableLiveData<String>()
    val saveStatus = MutableLiveData<OperationStatus>()
    private val playersStats = MutableLiveData<MutableList<PlayerMatchStats>>()

    @Inject
    lateinit var matchesRepository: MatchesRepository

    private var matchId: Int = -1
    private var teamId: Int = -1

    private lateinit var matchLineUp: MatchLineUp

    override fun init() {
        playersStats.value = mutableListOf()
        initRequest(GET_PLAYERS_FOR_TEAM_REQUEST_ID, object : Factory<Observable<TeamNewMatchModel>> {
            override fun create(): Observable<TeamNewMatchModel> {
                return matchesRepository.getPlayersForTeam(teamId)
            }
        },
                {
                    stop(GET_PLAYERS_FOR_TEAM_REQUEST_ID)
                    players.value = it.players.toMutableList()
                    team.value = it.team
                },
                {
                    stop(GET_PLAYERS_FOR_TEAM_REQUEST_ID)
                })
        initRequest(SAVE_LINEUP_REQUEST_ID, object : Factory<Observable<Int>> {
            override fun create(): Observable<Int> {
                return matchesRepository.saveMatchLineup(matchLineUp)
            }
        },
                {
                    stop(SAVE_LINEUP_REQUEST_ID)
                    playersStats.value!!.forEach { player -> player.matchId = it }
                    start(SAVE_PLAYERS_REQUEST_ID)
                },
                {
                    stop(SAVE_LINEUP_REQUEST_ID)
                })
        initRequest(SAVE_PLAYERS_REQUEST_ID, object : Factory<Observable<Unit>> {
            override fun create(): Observable<Unit> {
                return matchesRepository.savePlayers(playersStats.value!!)
            }
        },
                {
                    stop(SAVE_PLAYERS_REQUEST_ID)
                    saveStatus.value = OperationSuccess
                },
                {
                    stop(SAVE_PLAYERS_REQUEST_ID)
                    saveStatus.value = OperationError(it)
                })
    }

    fun loadDataForTeam(teamId: Int) {
        this.teamId = teamId
        start(GET_PLAYERS_FOR_TEAM_REQUEST_ID)
    }

    fun getEnabledPlayersForPosition(): List<Player> {
        val notEnabledIds = playersStats.value?.asSequence()?.map { stat -> stat.playerId }?.toList()
                ?: emptyList()
        return players.value!!.asSequence().filter { it -> !notEnabledIds.contains(it.id) }.toList()
    }

    fun removePlayerAtPosition(positionNumber: Int) {
        playersStats.value!!.remove(playersStats.value!!.first { it.position == positionNumber })
    }

    fun addPlayerAtPosition(player: Player, positionNumber: Int) {
        playersStats.value!!.add(PlayerMatchStats(matchId = matchId, playerId = player.id, position = positionNumber,
                assists = null, goals = null, inOutMin = null, redCard = null, yellowCards = null))
    }

    fun getDate() = TimeUtils.convertTimeToString(eventDate.value)

    fun getPlayerFullDetailsList(): List<PlayerFullDetails>? {
        return if (playersStats.value!!.isEmpty()) {
            null
        } else {
            playersStats.value!!.map { stat -> PlayerFullDetails(players.value!!.first { it.id == stat.playerId }, stat) }
        }
    }

    fun save() {
        matchLineUp = MatchLineUp(matchId = 0,
                teamId = teamId,
                jersey = jersey.value!!,
                lineup = lineup.value!!,
                opponentName = opponentName.value,
                time = eventDate.value)
    }

    fun firstSquadIsSet(): Boolean {
        return playersStats.value!!.filter { it.position <= 11 }.size == 11
    }

    companion object {
        private const val GET_PLAYERS_FOR_TEAM_REQUEST_ID = 6000
        private const val SAVE_LINEUP_REQUEST_ID = 6001
        private const val SAVE_PLAYERS_REQUEST_ID = 6002
    }
}