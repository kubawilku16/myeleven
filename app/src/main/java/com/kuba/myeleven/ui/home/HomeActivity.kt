package com.kuba.myeleven.ui.home

import android.content.Context
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.f2prateek.dart.InjectExtra
import com.kuba.myeleven.R
import com.kuba.myeleven.databinding.ActivityHomeBinding
import com.kuba.myeleven.ui.base.BaseActivity
import com.kuba.myeleven.ui.home.lienups.LineupsFragment
import com.kuba.myeleven.ui.home.teams.TeamsFragment
import icepick.State
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity<HomeViewModel, ActivityHomeBinding>(HomeViewModel::class.java) {

    override val screenTitle: String
        get() = ""
    override val layoutId: Int
        get() = R.layout.activity_home

    @InjectExtra
    @State
    @JvmField
    var showWelcomeDialog: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (showWelcomeDialog) {
            showWelcomeDialog = false
            getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE).edit().putBoolean("showWelcomeDialog", false).apply()
            //TODO show welcome dialog
        }
        initBottomNavigation()
        if (supportFragmentManager.findFragmentById(R.id.homeContainer) == null) {
            navigateToHomeFragment(false, 0)
        }
    }

    private fun initBottomNavigation() {
        homeBottomNavigation.setColoredModeColors(ContextCompat.getColor(this, R.color.secondDarkColor), ContextCompat.getColor(this, R.color.mainDarkColor))
        homeBottomNavigation.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
        homeBottomNavigation.accentColor = ContextCompat.getColor(this, R.color.white)
        homeBottomNavigation.setDefaultBackgroundResource(R.color.secondColor)
        homeBottomNavigation.isBehaviorTranslationEnabled = false
        homeBottomNavigation.addItem(AHBottomNavigationItem(R.string.home_nav_teams, R.drawable.ic_teams, R.color.white))
        homeBottomNavigation.addItem(AHBottomNavigationItem(R.string.home_nav_pitch, R.drawable.ic_pitch, R.color.white))
        homeBottomNavigation.setOnTabSelectedListener { position, wasSelected ->
            navigateToHomeFragment(wasSelected, position)
            true
        }
        homeBottomNavigation.currentItem = 0
    }

    private fun navigateToHomeFragment(wasSelected: Boolean, position: Int) {
        if (!wasSelected) {
            val fragment = when (position) {
                0 -> TeamsFragment()
                1 -> LineupsFragment()
                else -> null
            }
            if (fragment != null) {
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.homeContainer, fragment, fragment.tag)
                        .commit()
            }
        }
    }
}