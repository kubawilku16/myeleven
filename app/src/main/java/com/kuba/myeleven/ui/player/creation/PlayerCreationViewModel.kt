package com.kuba.myeleven.ui.player.creation

import androidx.lifecycle.MutableLiveData
import com.kuba.myeleven.data.entities.Player
import com.kuba.myeleven.data.enums.BetterLeg
import com.kuba.myeleven.data.enums.Position
import com.kuba.myeleven.repository.PlayersRepository
import com.kuba.myeleven.ui.base.BaseViewModel
import com.kuba.myeleven.ui.base.Factory
import com.kuba.myeleven.utils.OperationError
import com.kuba.myeleven.utils.OperationStatus
import com.kuba.myeleven.utils.OperationSuccess
import com.kuba.myeleven.utils.TimeUtils
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class PlayerCreationViewModel : BaseViewModel() {

    @Inject
    lateinit var playersRepository: PlayersRepository

    lateinit var player: Player

    var teamId = -1
    var playerId = -1

    val saveStatus = MutableLiveData<OperationStatus>()
    val name = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val number = MutableLiveData<Int>()
    val positions = MutableLiveData<MutableList<Position>>()
    val betterLeg = MutableLiveData<BetterLeg>()
    val birthDate = MutableLiveData<Date>()
    val teamMates = MutableLiveData<List<Player>>()

    override fun init() {
        initRequest(GET_PLAYER_DETAILS_REQUEST_ID, object : Factory<Observable<Player>> {
            override fun create(): Observable<Player> {
                return playersRepository.getPlayer(playerId)
                        .subscribeOn(mainExecutionThread.scheduler())
                        .observeOn(postExecutionThread.scheduler)
            }
        },
                {
                    stop(GET_PLAYER_DETAILS_REQUEST_ID)
                    name.value = it.firstName
                    lastName.value = it.lastName
                    number.value = it.number
                    betterLeg.value = it.betterLeg
                    birthDate.value = it.birthDay
                },
                {
                    stop(GET_PLAYER_DETAILS_REQUEST_ID)
                })
        initRequest(SAVE_PLAYER_REQUEST_ID, object : Factory<Observable<Unit>> {
            override fun create(): Observable<Unit> {
                return playersRepository.savePlayer(player)
                        .subscribeOn(mainExecutionThread.scheduler())
                        .observeOn(postExecutionThread.scheduler)
            }
        },
                {
                    stop(SAVE_PLAYER_REQUEST_ID)
                    saveStatus.value = OperationSuccess
                },
                {
                    stop(SAVE_PLAYER_REQUEST_ID)
                    saveStatus.value = OperationError(it)
                })
        initRequest(UPDATE_PLAYER_REQUEST_ID, object : Factory<Observable<Unit>> {
            override fun create(): Observable<Unit> {
                return playersRepository.updatePlayer(player)
                        .subscribeOn(mainExecutionThread.scheduler())
                        .observeOn(postExecutionThread.scheduler)
            }
        },
                {
                    stop(UPDATE_PLAYER_REQUEST_ID)
                    saveStatus.value = OperationSuccess
                },
                {
                    stop(UPDATE_PLAYER_REQUEST_ID)
                    saveStatus.value = OperationError(it)
                })
        initRequest(GET_TEAM_MATES_REQUEST_ID, object : Factory<Observable<List<Player>>> {
            override fun create(): Observable<List<Player>> {
                return playersRepository.getPlayers(teamId)
                        .subscribeOn(mainExecutionThread.scheduler())
                        .observeOn(postExecutionThread.scheduler)
            }
        },
                { players ->
                    stop(GET_TEAM_MATES_REQUEST_ID)
                    teamMates.value = players.minus(players.filter { it.id == playerId })
                },
                {
                    stop(GET_TEAM_MATES_REQUEST_ID)
                })
    }

    fun getDate() = TimeUtils.convertDateToString(birthDate.value)

    fun save(playerId: Int?) {
        if (playerId != null) {
            player = Player(id = playerId,
                    teamId = teamId,
                    number = number.value!!,
                    firstName = name.value!!,
                    lastName = lastName.value!!,
                    betterLeg = betterLeg.value!!,
                    birthDay = birthDate.value,
                    positions = positions.value!!)
            start(UPDATE_PLAYER_REQUEST_ID)
        } else {
            player = Player(
                    teamId = teamId,
                    number = number.value!!,
                    firstName = name.value!!,
                    lastName = lastName.value!!,
                    betterLeg = betterLeg.value!!,
                    birthDay = birthDate.value,
                    positions = positions.value!!)
            start(SAVE_PLAYER_REQUEST_ID)
        }
    }

    fun getDetails(playerId: Int) {
        this.playerId = playerId
        start(GET_PLAYER_DETAILS_REQUEST_ID)
    }

    fun getTeamMates(teamId: Int) {
        this.teamId = teamId
        start(GET_TEAM_MATES_REQUEST_ID)
    }

    fun onSwitchChange(position: Position) {
        if (positions.value == null) {
            positions.value = mutableListOf()
        }
        if (positions.value!!.contains(position)) {
            positions.value!!.remove(position)
        } else {
            positions.value!!.add(position)
        }
    }

    fun onLegChange(checked: Boolean) {
        betterLeg.value = if(checked) BetterLeg.RIGHT else BetterLeg.LEFT
    }

    companion object {
        private const val GET_PLAYER_DETAILS_REQUEST_ID = 4000
        private const val SAVE_PLAYER_REQUEST_ID = 4001
        private const val UPDATE_PLAYER_REQUEST_ID = 4002
        private const val GET_TEAM_MATES_REQUEST_ID = 4003
    }
}