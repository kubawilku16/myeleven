package com.kuba.myeleven.ui.player.creation

import androidx.fragment.app.FragmentActivity
import dagger.Binds
import dagger.Module

@Module
abstract class PlayerCreationModule {

    @Binds
    abstract fun activity(playerCreationActivity: PlayerCreationActivity): FragmentActivity
}