package com.kuba.myeleven.ui.team.manage.matches

import androidx.recyclerview.widget.LinearLayoutManager
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import com.kuba.myeleven.R
import com.kuba.myeleven.data.entities.MatchLineUp
import com.kuba.myeleven.databinding.FragmentTeamMatchesBinding
import com.kuba.myeleven.ext.toast
import com.kuba.myeleven.ui.base.BaseFragment
import com.kuba.myeleven.ui.views.list.BaseDelegationListAdapter
import com.kuba.myeleven.utils.RecyclerScrollFabListener
import kotlinx.android.synthetic.main.fragment_team_matches.*

@FragmentWithArgs
class TeamMatchesFragment : BaseFragment<TeamMatchesViewModel, FragmentTeamMatchesBinding>(TeamMatchesViewModel::class.java), TeamMatchesViewModel.TeamMatchesView {
    override val layoutId: Int
        get() = R.layout.fragment_team_matches

    override val screenTitle: String
        get() = ""
    @Arg
    var teamId: Int = -1

    private lateinit var adapter: BaseDelegationListAdapter

    override fun initView() {
        initRecycler()
        teamMatchesFab.setOnClickListener {
            navigator.goToTeamLineupCreationActivity(teamId)
        }
        model.loadLineups(teamId)
    }

    private fun initRecycler() {
        adapter = BaseDelegationListAdapter()
        teamMatchesList.setEmptyView(teamMatchesPlaceholder)
        teamMatchesList.layoutManager = LinearLayoutManager(context)
        teamMatchesList.adapter = adapter
        teamMatchesList.addOnScrollListener(RecyclerScrollFabListener(teamMatchesFab))
    }

    override fun onLineupsLoadSuccess(result: List<MatchLineUp>) {
        adapter.setReloadedList(result)
    }

    override fun onLineupsLoadError(throwable: Throwable) {
        context?.toast("LOAD ERROR")
    }
}
