package com.kuba.myeleven.ui.views

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView

class SquareImageView : ImageView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attributesSet: AttributeSet) : super(context, attributesSet)
    constructor(context: Context, attributesSet: AttributeSet, defStyleAttr: Int) : super(context, attributesSet, defStyleAttr)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec)
        clipToOutline = true
    }
}