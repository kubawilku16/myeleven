package com.kuba.myeleven.ui.player.creation

import android.app.Activity
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.lifecycle.Observer
import com.f2prateek.dart.InjectExtra
import com.kuba.myeleven.R
import com.kuba.myeleven.data.enums.BetterLeg
import com.kuba.myeleven.data.enums.Position
import com.kuba.myeleven.databinding.ActivityPlayerCreationBinding
import com.kuba.myeleven.ext.gone
import com.kuba.myeleven.ext.toast
import com.kuba.myeleven.ui.base.BaseActivity
import com.kuba.myeleven.ui.views.shared.SpinnerAdapter
import com.kuba.myeleven.utils.OperationError
import com.kuba.myeleven.utils.OperationStatus
import com.kuba.myeleven.utils.OperationSuccess
import com.kuba.myeleven.utils.TimeUtils
import icepick.State
import kotlinx.android.synthetic.main.activity_player_creation.*
import java.util.*

class PlayerCreationActivity : BaseActivity<PlayerCreationViewModel, ActivityPlayerCreationBinding>(PlayerCreationViewModel::class.java) {
    override val layoutId: Int
        get() = R.layout.activity_player_creation

    override val screenTitle: String
        get() = getString(R.string.player_creation_title)
    @State
    @InjectExtra
    @JvmField
    var teamId: Int? = null

    @State
    @InjectExtra
    @JvmField
    var playerId: Int? = null

    @State
    @JvmField
    var initialNumberSet = false

    lateinit var adapter: SpinnerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initListeners()
        initObservers()
        binding.viewModel = model
        if (playerId != null) {
            model.getDetails(playerId!!)
        } else {
            model.betterLeg.value = BetterLeg.RIGHT
        }
        if (teamId != null) {
            model.getTeamMates(teamId!!)
        } else {
            playerCreationNumberLayout.gone()
        }
    }

    private fun initObservers() {
        model.positions.observe(this, Observer {
            initCheckers(it)
        })
        model.teamMates.observe(this, Observer { initNumberSpinner() })
        model.saveStatus.observe(this, Observer { onSaveStatusChanged(it) })
    }

    private fun onSaveStatusChanged(status: OperationStatus?) {
        when (status) {
            is OperationSuccess -> {
                navigator.finish(result = Activity.RESULT_OK, intent = intent)
            }
            is OperationError -> {
                applicationContext.toast("SAVE ERROR: ${status.throwable.message}")
            }
        }
    }

    private fun initNumberSpinner() {
        adapter = object : SpinnerAdapter(applicationContext, R.layout.list_item_spinner_number, IntRange(1, 99).toList()) {
            override fun enabled(item: Any): Boolean {
                return !model.teamMates.value!!.asSequence().map { it.number }.minus(model.number.value).contains(item as Int)
            }
        }
        playerCreationNumberSpinner.adapter = adapter
        playerCreationNumberSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (model.teamMates.value!!.map { it.number }.contains(adapter.getItem(p2)!!)) {
                    if (initialNumberSet) {
                        applicationContext.toast(R.string.error_number_reserved)
                    }
                    initialNumberSet = true
                    model.number.value = IntRange(1, 99).toList().minus(model.teamMates.value!!.map { it.number }).first()
                    playerCreationNumberSpinner.setSelection(model.number.value!! - 1)
                } else {
                    model.number.value = adapter.getItem(p2) as Int
                }
            }
        }
        playerCreationNumberSpinner.setSelection(IntRange(1, 99).toList()
                .indexOf(model.number.value ?: 0))
    }

    private fun initListeners() {
        playerCreationCalendar.setOnClickListener {
            val cal = Calendar.getInstance()
            if (model.birthDate.value != null) {
                cal.time = model.birthDate.value
            }
            val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, yy, mm, dd ->
                model.birthDate.value = TimeUtils.convertToDate("$dd-${mm + 1}-$yy")
                playerCreationDate.setText(model.getDate(), TextView.BufferType.EDITABLE)
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
            dialog.datePicker.maxDate = System.currentTimeMillis()
            dialog.show()
        }
        playerCreationSaveBtn.setOnClickListener {
            try {
                validate()
                model.save(playerId)
            } catch (e: Exception) {
                showNativeMessage(title = getString(R.string.common_error_occured),
                        message = e.message!!,
                        confirmButtonText = getString(R.string.common_ok),
                        dialogCancelable = true,
                        drawableResId = R.drawable.ic_soccer_error)
            }
        }
        playerCreationDate.setOnFocusChangeListener { _, focused ->
            if(!focused && playerCreationDate.text.isNotEmpty() &&!playerCreationDate.text.toString().matches(Regex("([0-9]{2})-([0-9]{2})-([0-9]{4})"))){
                playerCreationDate.setText("")
                applicationContext.toast(R.string.error_incorrect_date_format)
            }
        }
        playerCreationDate.setOnEditorActionListener { _, _, _ ->
            if (!playerCreationDate.text.toString().matches(Regex("([0-9]{2})-([0-9]{2})-([0-9]{4})"))) {
                playerCreationDate.setText("")
                applicationContext.toast(R.string.error_incorrect_date_format)
            }
            false
        }
        playerCreationGoalkeeper.setOnCheckedChangeListener { _, _ -> model.onSwitchChange(Position.GOALKEEPER) }
        playerCreationDefender.setOnCheckedChangeListener { _, _ -> model.onSwitchChange(Position.DEFENDER) }
        playerCreationMidfielder.setOnCheckedChangeListener { _, _ -> model.onSwitchChange(Position.MIDFIELDER) }
        playerCreationStriker.setOnCheckedChangeListener { _, _ -> model.onSwitchChange(Position.STRIKER) }
    }

    private fun validate() {
        with(model) {
            if (playerCreationName.text.isEmpty()) {
                throw Exception(getString(R.string.error_player_name_empty))
            }
            if (playerCreationLastName.text.isEmpty()) {
                throw Exception(getString(R.string.error_player_last_name_empty))
            }
            if (positions.value == null || positions.value!!.isEmpty()) {
                throw Exception(getString(R.string.error_player_postions))
            }
        }
    }

    private fun initCheckers(positions: MutableList<Position>) {
        if (positions.contains(Position.GOALKEEPER)) {
            playerCreationGoalkeeper.isChecked = true
        }
        if (positions.contains(Position.DEFENDER)) {
            playerCreationDefender.isChecked = true
        }
        if (positions.contains(Position.MIDFIELDER)) {
            playerCreationMidfielder.isChecked = true
        }
        if (positions.contains(Position.STRIKER)) {
            playerCreationStriker.isChecked = true
        }
    }
}
