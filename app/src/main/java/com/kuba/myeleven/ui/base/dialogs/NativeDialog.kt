package com.kuba.myeleven.ui.base.dialogs

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.text.Html
import com.afollestad.materialdialogs.MaterialDialog
import com.hannesdorfmann.fragmentargs.annotation.Arg

class NativeDialog : BaseDialog() {

    @Arg
    lateinit var title: String

    @Arg
    lateinit var message: String

    @Arg(required = false)
    var positiveText: String = ""

    @Arg(required = false)
    var negativeText: String = ""

    @Arg(required = false)
    var drawableRes: Int = -1

    @Arg
    var closeable: Boolean = true

    @Arg
    var requestCode: Int = 0

    private var dialogListener: NativeDialogListener? = null

    interface NativeDialogListener {
        fun onPopupPositiveButtonClicked(requestCode: Int)
        fun onPopupNegativeButtonClicked(requestCode: Int)
    }

    @Suppress("DEPRECATION")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = MaterialDialog.Builder(activity!!)
        builder.run {
            if (drawableRes != -1) {
                iconRes(drawableRes)
            }
            title(title)
            if (positiveText.isNotEmpty()) {
                positiveText(positiveText)
                onPositive { _, _ ->
                    if (activity is NativeDialogListener) {
                        (activity as NativeDialogListener).onPopupPositiveButtonClicked(requestCode)
                    } else if (dialogListener != null) {
                        dialogListener!!.onPopupPositiveButtonClicked(requestCode)
                    }
                }
            }
            if (negativeText.isNotEmpty()) {
                negativeText(negativeText)
                onNegative { _, _ ->
                    if (activity is NativeDialogListener) {
                        (activity as NativeDialogListener).onPopupNegativeButtonClicked(requestCode)
                    } else if (dialogListener != null) {
                        dialogListener!!.onPopupNegativeButtonClicked(requestCode)
                    }
                }
            }
            cancelable(closeable)
            canceledOnTouchOutside(closeable)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                content(Html.fromHtml(message, Html.FROM_HTML_MODE_LEGACY))
            } else {
                content(Html.fromHtml(message))
            }
        }
        return builder.show()
    }

    fun setDialogListener(dialogListener: NativeDialogListener) {
        this.dialogListener = dialogListener
    }

    companion object {
        const val TAG = "error_dialog_tag"
    }
}