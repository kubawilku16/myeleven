package com.kuba.myeleven.ui.home.teams

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.kuba.myeleven.R
import com.kuba.myeleven.data.entities.Team
import com.kuba.myeleven.databinding.FragmentTeamsBinding
import com.kuba.myeleven.ext.toast
import com.kuba.myeleven.ui.base.BaseFragment
import com.kuba.myeleven.ui.base.navigation.Navigator
import com.kuba.myeleven.ui.views.CustomBottomSheet
import com.kuba.myeleven.ui.views.list.BaseDelegationListAdapter
import com.kuba.myeleven.utils.RecyclerScrollFabListener
import icepick.State
import kotlinx.android.synthetic.main.fragment_teams.*

class TeamsFragment : BaseFragment<TeamsViewModel, FragmentTeamsBinding>(TeamsViewModel::class.java), TeamsViewModel.TeamsView {
    override val layoutId: Int
        get() = R.layout.fragment_teams

    override val screenTitle: String
        get() = ""

    @State
    @JvmField
    var selectedTeamId = 0

    @State
    @JvmField
    var selectedTeamColor = 0

    @State
    @JvmField
    var bottomSheetState = BottomSheetBehavior.STATE_HIDDEN

    private lateinit var adapter: BaseDelegationListAdapter
    private lateinit var sheetBehavior: BottomSheetBehavior<*>

    override fun initView() {
        model.view = this
        initRecyclerView()
        model.loadTeams()
        initBottomSheet()
        teamsFab.setOnClickListener {
            navigator.goToTeamCreationActivity()
        }
    }

    private fun initRecyclerView() {
        adapter = BaseDelegationListAdapter()
        adapter.addDelegate(TeamDelegate(object : TeamDelegate.OnClickListener {
            override fun onLonClick(id: Int, name: String) {
                if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                    selectedTeamId = id
                    teamsBottomSheet.setBottomSheetTitle(name)
                    sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                } else {
                    sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                }
            }

            override fun onClick(id: Int, name: String, colorRes: Int) {
                if (sheetBehavior.state != BottomSheetBehavior.STATE_HIDDEN) {
                    sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                }
                navigator.goToManageTeamActivity(id, name, colorRes)
            }
        }))
        teamsList.setEmptyView(teamsPlaceholder)
        teamsList.layoutManager = GridLayoutManager(context, 2)
        teamsList.adapter = adapter
        teamsList.addOnScrollListener(RecyclerScrollFabListener(teamsFab))
    }

    private fun initBottomSheet() {
        sheetBehavior = BottomSheetBehavior.from(teamsBottomSheet)
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                bottomSheetState = newState
            }
        })
        teamsBottomSheet.clickListener = object : CustomBottomSheet.OnClickListener {
            override fun onDeleteClick() {
                sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                model.delete(selectedTeamId)
            }

            override fun onEditClick() {
                navigator.goToTeamCreationActivity(selectedTeamId)
                sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == Navigator.SAVE_TEAM_SUCCESS_REQUEST_CODE) {
            model.loadTeams()
        }
    }

    override fun onLoadSuccess(result: List<Team>) {
        adapter.setReloadedList(result)
    }

    override fun onLoadError(throwable: Throwable) {
        context?.toast("LOAD ERROR")
    }


    override fun onDeleteSuccess() {
        val removedPosition = adapter.items.indexOfFirst { (it is Team && it.id == selectedTeamId) }
        adapter.items.removeAt(removedPosition)
        adapter.notifyItemRemoved(removedPosition)
        adapter.notifyItemRangeRemoved(removedPosition, adapter.items.size - 1)
    }

    override fun onDeleteError(throwable: Throwable) {
        context?.toast("DELETE ERROR")

    }
}
