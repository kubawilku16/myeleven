package com.kuba.myeleven.ui.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kuba.myeleven.executors.MainExecutionThread
import com.kuba.myeleven.executors.PostExecutionThread
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject


open class BaseViewModel : ViewModel() {

    @Inject
    lateinit var mainExecutionThread: MainExecutionThread

    @Inject
    lateinit var postExecutionThread: PostExecutionThread

    var isUiLocked = MutableLiveData<Boolean>()
    private var compositeDisposable = CompositeDisposable()
    private val disposables = mutableMapOf<Int, Factory<Disposable>>()
    private val requested = mutableMapOf<Int, Disposable>()
    private var startedIds = mutableListOf<Int>()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
        compositeDisposable.dispose()
    }

    protected fun start(requestId: Int) {
        if (startedIds.contains(requestId)) {
            stop(requestId)
        }
        startedIds.add(requestId)
        requested[requestId] = disposables[requestId]!!.create()
        compositeDisposable.add(requested[requestId]!!)
    }

    protected fun stop(requestId: Int) {
        requested[requestId]?.dispose()
        startedIds.remove(requestId)
    }

    protected fun <T> initRequest(requestId: Int, factory: Factory<Observable<T>>, onNext: (T) -> (Unit), onError: (Throwable) -> Unit) {
        disposables[requestId] = object : Factory<Disposable> {
            override fun create(): Disposable {
                return factory
                        .create()
                        .compose {
                            it
                                    .subscribeOn(mainExecutionThread.scheduler())
                                    .observeOn(postExecutionThread.scheduler)
                        }
                        .subscribe(onNext, onError)
            }

        }
    }

    fun resume(ids: MutableList<Int>) {
        ids.filter { !startedIds.contains(it) }
                .forEach {
                    requested[it] = disposables[it]!!.create()
                    compositeDisposable.add(requested[it]!!)
                }
        this.startedIds = ids
    }

    fun getDisposablesIdsToSave() = startedIds

    open fun init() {}
}