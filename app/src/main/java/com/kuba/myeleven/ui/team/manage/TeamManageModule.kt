package com.kuba.myeleven.ui.team.manage

import androidx.fragment.app.FragmentActivity
import com.kuba.myeleven.ui.player.details.PlayerDetailsFragment
import com.kuba.myeleven.ui.player.details.PlayerDetailsModule
import com.kuba.myeleven.ui.team.manage.matches.TeamMatchesFragment
import com.kuba.myeleven.ui.team.manage.matches.TeamMatchesModule
import com.kuba.myeleven.ui.team.manage.players.TeamPlayersFragment
import com.kuba.myeleven.ui.team.manage.players.TeamPlayersModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TeamManageModule {

    @Binds
    abstract fun activity(teamManageActivity: TeamManageActivity): FragmentActivity

    @ContributesAndroidInjector(modules = [(TeamMatchesModule::class)])
    abstract fun teamMatchesFragment(): TeamMatchesFragment

    @ContributesAndroidInjector(modules = [(TeamPlayersModule::class)])
    abstract fun teamPlayersFragment(): TeamPlayersFragment

    @ContributesAndroidInjector(modules = [(PlayerDetailsModule::class)])
    abstract fun playerDetailsFragment(): PlayerDetailsFragment
}