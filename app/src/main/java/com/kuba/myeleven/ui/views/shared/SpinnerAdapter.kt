package com.kuba.myeleven.ui.views.shared

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.kuba.myeleven.R

abstract class SpinnerAdapter(context: Context, var layoutId: Int, items: List<Any>) : ArrayAdapter<Any>(context, layoutId, items) {

    abstract fun enabled(item: Any): Boolean

    override fun isEnabled(position: Int): Boolean {
        return enabled(getItem(position)!!)
    }

    @SuppressLint("ViewHolder", "ResourceAsColor")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = LayoutInflater.from(context).inflate(layoutId, parent, false)
        if (!isEnabled(position)) {
            view.findViewById<TextView>(R.id.listSpinnerNumber).typeface = Typeface.DEFAULT
            view.findViewById<TextView>(R.id.listSpinnerNumber).setTextColor(R.color.Gray)
        }
        return super.getView(position, view, parent)
    }

    @SuppressLint("ResourceAsColor")
    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = LayoutInflater.from(context).inflate(layoutId, parent, false)
        if (!isEnabled(position)) {
            view.findViewById<TextView>(R.id.listSpinnerNumber).typeface = Typeface.DEFAULT
            view.findViewById<TextView>(R.id.listSpinnerNumber).setTextColor(R.color.Gray)
        }
        return super.getDropDownView(position, view, parent)
    }
}