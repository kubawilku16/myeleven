package com.kuba.myeleven.ui.home

import androidx.fragment.app.FragmentActivity
import com.kuba.myeleven.ui.home.lienups.LineupsFragment
import com.kuba.myeleven.ui.home.lienups.LineupsModule
import com.kuba.myeleven.ui.home.teams.TeamsFragment
import com.kuba.myeleven.ui.home.teams.TeamsModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeModule {

    @Binds
    abstract fun activity(homeActivity: HomeActivity): FragmentActivity

    @ContributesAndroidInjector(modules = [(TeamsModule::class)])
    abstract fun teamsFragment(): TeamsFragment

    @ContributesAndroidInjector(modules = [(LineupsModule::class)])
    abstract fun lineupsFragment(): LineupsFragment

}