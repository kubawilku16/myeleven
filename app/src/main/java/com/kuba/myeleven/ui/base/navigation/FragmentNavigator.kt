package com.kuba.myeleven.ui.base.navigation

import android.content.Intent
import androidx.fragment.app.Fragment
import javax.inject.Inject

class FragmentNavigator @Inject constructor(private var fragment: Fragment): Navigator(fragment.activity!!) {

    override fun startActivityForResult(intent: Intent, requestCode: Int) {
        fragment.startActivityForResult(intent, requestCode)
    }

    override fun startActivity(intent: Intent) {
        fragment.startActivity(intent)
    }
}