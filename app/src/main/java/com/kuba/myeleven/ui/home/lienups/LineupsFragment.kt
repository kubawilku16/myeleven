package com.kuba.myeleven.ui.home.lienups

import com.kuba.myeleven.R
import com.kuba.myeleven.databinding.FragmentLineupsBinding
import com.kuba.myeleven.ui.base.BaseFragment

class LineupsFragment : BaseFragment<LineupsViewModel, FragmentLineupsBinding>(LineupsViewModel::class.java) {
    override val layoutId: Int
        get() = R.layout.fragment_lineups
    override val screenTitle: String
        get() = ""

    override fun initView() {

    }
}