package com.kuba.myeleven.ui.views.list

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kuba.myeleven.ui.views.shared.ItemProgressListViewDelegate
import com.kuba.myeleven.ui.views.shared.ListProgress
import com.kuba.myeleven.utils.CustomBundler
import icepick.Icepick

/**
 * Custom implementation of [RecyclerView]
 */
class CustomRecyclerView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    private var containerView: View? = null
    private var emptyView: View? = null

    private var scrolledDownListener: ScrolledDownListener? = null

    @icepick.State
    @JvmField
    var isLoadingAllFinished: Boolean = false

    @icepick.State
    @JvmField
    var isLoadingMore: Boolean = false

    @icepick.State(CustomBundler::class)
    @JvmField
    var itemProgress: ListProgress? = null

    init {
        if (!isInEditMode) {
            addOnScrollListener(object : OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    post {
                        if (isLastItemDisplaying() && !isLoadingAllFinished && !isLoadingMore && scrolledDownListener != null) {
                            loadingMore()
                            scrolledDownListener?.viewIsOnTheBottom()
                        }
                    }

                }
            })
        }
    }

    /**
     * Observer on data changes
     */
    private val observer = object : RecyclerView.AdapterDataObserver() {
        override fun onChanged() {
            checkIfEmpty()
        }

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            checkIfEmpty()
        }

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
            checkIfEmpty()
        }
    }

    override fun setAdapter(adapter: RecyclerView.Adapter<*>?) {
        val oldAdapter = getAdapter()
        oldAdapter?.unregisterAdapterDataObserver(observer)
        if (adapter is BaseDelegationListAdapter) {
            if (adapter.delegates.singleOrNull { it is ItemProgressListViewDelegate } == null) {
                adapter.addDelegate(ItemProgressListViewDelegate())
            }
        }
        super.setAdapter(adapter)
        adapter?.registerAdapterDataObserver(observer)
        adapter?.let {
            checkIfEmpty()
        }
    }

    public override fun onSaveInstanceState(): Parcelable? {
        return Icepick.saveInstanceState(this, super.onSaveInstanceState())
    }

    public override fun onRestoreInstanceState(state: Parcelable) {
        super.onRestoreInstanceState(Icepick.restoreInstanceState(this, state))
    }

    /**
     * Checks if list of elements is empty, if is empty then hides recyclerview
     */
    fun checkIfEmpty() {
        if (emptyView != null && adapter != null) {
            val emptyViewVisible = adapter!!.itemCount == 0
            emptyView?.visibility = if (emptyViewVisible) View.VISIBLE else View.GONE
            if (containerView != null) {
                containerView?.visibility = if (emptyViewVisible) View.GONE else View.VISIBLE
            } else {
                visibility = if (emptyViewVisible) View.GONE else View.VISIBLE
            }
        }
    }

    /**
     * Sets empty view for showing if list of elements will be empty
     */
    fun setEmptyView(emptyView: View?) {
        this.emptyView = emptyView
        checkIfEmpty()
    }

    fun setRecyclerViewContainer(containerView: View) {
        this.containerView = containerView
    }

    /**
     * Checks if last item was displayed
     *
     * @return true if there is no more elements to show, false otherwise
     */
    fun isLastItemDisplaying(): Boolean {
        if (this.adapter!!.itemCount != 0) {
            var lastVisibleItemPosition = 0
            if (this.layoutManager is LinearLayoutManager) {
                lastVisibleItemPosition = (this.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
            } else if (this.layoutManager is GridLayoutManager) {
                lastVisibleItemPosition = (this.layoutManager as GridLayoutManager).findLastCompletelyVisibleItemPosition()
            }
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == this.adapter!!.itemCount - 1)
                return true
        }
        return false
    }

    fun setScrollDownListener(scrolledDownListener: ScrolledDownListener?) {
        this.scrolledDownListener = scrolledDownListener
    }

    /**
     * @interface for scrolling listening
     */
    interface ScrolledDownListener {
        fun viewIsOnTheBottom()
    }

    /**
     * Determines to show new loaded items
     */
    fun loadingMore() {
        isLoadingMore = true
        adapter?.let {
            if (it.itemCount > 0) {
                when (it) {
                    is BaseDelegationListAdapter -> {
                        it.items = listOf(provideItemProgress())
                        layoutManager?.run {
                            scrollToPosition(it.items.size - 1)
                        }
                    }
                }
            }
        }
    }

    /**
     * Hides loading progress if is still visible after end of loading new elements
     */
    fun loadingFinished(isLoadingAllFinished: Boolean) {
        adapter?.let {
            if (it is BaseDelegationListAdapter) {
                if (it.items != null) {
                    val pos = it.items.indexOfFirst { it is ListProgress }
                    if (pos > -1) {
                        it.removeItem(pos)
                    }
                }
            }
        }
        isLoadingMore = false
        this.isLoadingAllFinished = isLoadingAllFinished
    }

    /**
     * Provides item progress
     *
     * @return [ListProgress] if is null then create new, else return old progress.
     */
    private fun provideItemProgress(): ListProgress {
        if (itemProgress == null) {
            itemProgress = ListProgress()
        }
        return itemProgress!!
    }

    /**
     * Function clears references to listeners to remove memory leaks
     */
    fun clearReferences() {
        scrolledDownListener = null
        clearOnScrollListeners()
        emptyView = null
        containerView = null
        adapter = null
    }
}