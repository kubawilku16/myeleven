package com.kuba.myeleven.ui.views

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.PorterDuff
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.kuba.myeleven.R
import com.kuba.myeleven.data.entities.Player
import com.kuba.myeleven.data.enums.Jersey
import com.kuba.myeleven.data.enums.Position
import icepick.Icepick
import icepick.State
import kotlinx.android.synthetic.main.widget_player.view.*

class PlayerWidget : FrameLayout {

    @State
    @JvmField
    var shirtWidth = (Resources.getSystem().displayMetrics.widthPixels / 8)

    @State
    @JvmField
    var number = 0

    @State
    @JvmField
    var nameVisibility = View.VISIBLE

    @State
    @JvmField
    var name = ""

    @State
    lateinit var position: Position

    @State
    @JvmField
    var positionNumber = -1

    @State
    lateinit var jersey: Jersey

    constructor(context: Context) : super(context) {
        initView()
    }

    constructor(context: Context, attributesSet: AttributeSet) : super(context, attributesSet) {
        loadAttrs(context, attributesSet, R.attr.playerWidget)
        initView()
    }

    constructor(context: Context, attributesSet: AttributeSet, defStyleAttr: Int) : super(context, attributesSet, defStyleAttr) {
        loadAttrs(context, attributesSet, defStyleAttr)
        initView()
    }

    private fun loadAttrs(context: Context, attrs: AttributeSet, defStyle: Int) {
        with(context.obtainStyledAttributes(attrs, R.styleable.PlayerWidget, defStyle, 0)) {
            number = this.getInteger(R.styleable.PlayerWidget_playerNumber, 10)
            nameVisibility = when (this.getInteger(R.styleable.PlayerWidget_nameVisibility, nameVisibility)) {
                0 -> View.VISIBLE
                1 -> View.GONE
                else -> View.VISIBLE
            }
            this.recycle()
        }
    }

    override fun onSaveInstanceState(): Parcelable? {
        return Icepick.saveInstanceState(this, super.onSaveInstanceState())
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        super.onRestoreInstanceState(Icepick.restoreInstanceState(this, state))
    }

    private fun initView() {
        View.inflate(context, R.layout.widget_player, this)
        playerShirt.layoutParams = ConstraintLayout.LayoutParams(shirtWidth, shirtWidth)
        playerShirtStroke.layoutParams = ConstraintLayout.LayoutParams(shirtWidth, shirtWidth)
        bindView()
        playerName.visibility = nameVisibility
    }

    private fun bindView() {
        playerNumber.text = "$number"
        playerName.text = name
    }

    fun setPlayer(player: Player?) {
        this.number = player?.number ?: 0
        this.name = if (player != null) {
            "${player.firstName[0]}. ${player.lastName}"
        } else {
            ""
        }
        bindView()
    }

    fun setOnPitchListener(listener: PitchWidget.OnPlayerClickListener?) {
        playerRoot.setOnClickListener { listener?.onClick(position, positionNumber, this) }
    }

    fun setOnBenchListener(listener: BenchWidget.OnPlayerClickListener?){
        playerRoot.setOnClickListener { listener?.onClick(positionNumber, this) }

    }

    @SuppressLint("ResourceType")
    fun setAndBindJersey(jersey: Jersey) {
        this.jersey = jersey
        playerNumber.setTextColor(ContextCompat.getColor(context, jersey.numberColor))
        playerShirt.setColorFilter(ContextCompat.getColor(context, jersey.mainColor), PorterDuff.Mode.SRC_IN)
    }
}