package com.kuba.myeleven.ui.views

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kuba.myeleven.R
import com.kuba.myeleven.ui.views.list.BaseAdapterDelegate
import kotlinx.android.extensions.LayoutContainer

class AddItemDelegate(val clickListener: OnClickListener) : BaseAdapterDelegate<Unit, AddItemDelegate.ViewHolder>(Unit::class.java) {
    override val layoutId: Int
        get() = R.layout.list_item_add

    override fun createViewHolder(view: View): ViewHolder = ViewHolder(view)

    override fun bindViewHolder(item: Unit, holder: ViewHolder, position: Int, itemsSize: Int) {
        holder.bind()
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind() {
            containerView.setOnClickListener { clickListener.onClick() }
        }
    }

    interface OnClickListener {
        fun onClick()
    }
}