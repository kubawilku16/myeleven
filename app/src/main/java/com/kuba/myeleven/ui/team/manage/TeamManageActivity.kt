package com.kuba.myeleven.ui.team.manage

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.f2prateek.dart.InjectExtra
import com.kuba.myeleven.R
import com.kuba.myeleven.databinding.ActivityTeamManageBinding
import com.kuba.myeleven.ext.gone
import com.kuba.myeleven.ext.visible
import com.kuba.myeleven.ui.base.BaseActivity
import com.kuba.myeleven.ui.team.manage.matches.TeamMatchesFragmentBuilder
import com.kuba.myeleven.ui.team.manage.players.TeamPlayersFragmentBuilder
import kotlinx.android.synthetic.main.activity_team_manage.*
import kotlinx.android.synthetic.main.custom_toolbar.*

class TeamManageActivity : BaseActivity<TeamManageViewModel, ActivityTeamManageBinding>(TeamManageViewModel::class.java) {
    override val layoutId: Int
        get() = R.layout.activity_team_manage
    override val screenTitle: String by lazy { teamName }

    @InjectExtra
    lateinit var teamName: String

    @InjectExtra
    @JvmField
    var teamId: Int = -1

    @InjectExtra
    @JvmField
    var mainColor: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBottomNavigation()
        if (supportFragmentManager.findFragmentById(R.id.teamManageContainer) == null) {
            navigate(false, 0)
        }
    }

    private fun initBottomNavigation() {
        teamManageNavigation.setColoredModeColors(ContextCompat.getColor(this, R.color.secondDarkColor), ContextCompat.getColor(this, R.color.mainDarkColor))
        teamManageNavigation.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
        teamManageNavigation.accentColor = ContextCompat.getColor(this, R.color.white)
        teamManageNavigation.setDefaultBackgroundResource(R.color.secondColor)
        teamManageNavigation.isBehaviorTranslationEnabled = false
        teamManageNavigation.addItem(AHBottomNavigationItem(R.string.team_manage_nav_players, R.drawable.ic_player, R.color.white))
        teamManageNavigation.addItem(AHBottomNavigationItem(R.string.team_manage_nav_matches, R.drawable.ic_pitch, R.color.white))
        teamManageNavigation.setOnTabSelectedListener { position, wasSelected ->
            navigate(wasSelected, position)
            true
        }
        teamManageNavigation.currentItem = 0
    }

    private fun navigate(wasSelected: Boolean, position: Int) {
        if (!wasSelected) {
            val fragment = when (position) {
                0 -> TeamPlayersFragmentBuilder(mainColor, teamId, teamName).build()
                1 -> TeamMatchesFragmentBuilder(teamId).build()
                else -> null
            }
            if (fragment != null) {
                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.teamManageContainer, fragment, fragment.tag)
                        .commit()
            }
        }
    }


    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount >= 1) {
            supportFragmentManager.popBackStack()
            teamManageNavigation.visible()
            teamManageContainer.setPadding(0, 0, 0, resources.getDimension(R.dimen.toolbarHeight).toInt())
            teamManageContainer.invalidate()
        } else {
            super.onBackPressed()
        }
    }

    fun prepareToolbar(title: String? = null,
                       shareListener: (() -> Unit)? = null,
                       deleteListener: (() -> Unit)? = null,
                       editListener: (() -> Unit)? = null) {
        title?.let {
            toolbarTitle.text = title
        }
        if (shareListener != null) {
            if (!toolbarShareBtn.isExpanded)
                toolbarShareBtn.expand()
            toolbarShareBtn.setOnClickListener { shareListener() }
        } else {
            if (toolbarShareBtn.isExpanded)
                toolbarShareBtn.toggle()
        }
        if (editListener != null) {
            if (!toolbarEditBtn.isExpanded)
                toolbarEditBtn.expand()
            toolbarEditBtn.setOnClickListener { editListener() }
        } else {
            if (toolbarEditBtn.isExpanded)
                toolbarEditBtn.toggle()
        }
        if (deleteListener != null) {
            if (!toolbarDeleteBtn.isExpanded)
                toolbarDeleteBtn.expand()
            toolbarDeleteBtn.setOnClickListener { deleteListener() }
        } else {
            if (toolbarDeleteBtn.isExpanded)
                toolbarDeleteBtn.toggle()
        }
    }

    fun hideNavigation() {
        teamManageContainer.setPadding(0, 0, 0, 0)
        teamManageNavigation.gone()
    }
}