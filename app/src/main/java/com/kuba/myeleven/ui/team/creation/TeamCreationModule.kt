package com.kuba.myeleven.ui.team.creation

import androidx.fragment.app.FragmentActivity
import dagger.Binds
import dagger.Module

@Module
abstract class TeamCreationModule {

    @Binds
    abstract fun activity(teamCreationActivity: TeamCreationActivity): FragmentActivity
}