package com.kuba.myeleven.ui.team.creation

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.f2prateek.dart.InjectExtra
import com.kuba.myeleven.R
import com.kuba.myeleven.data.enums.Jersey
import com.kuba.myeleven.databinding.ActivityTeamCreationBinding
import com.kuba.myeleven.ext.toast
import com.kuba.myeleven.ui.base.BaseActivity
import com.kuba.myeleven.ui.team.selector.JerseySelectorDialog
import com.kuba.myeleven.ui.team.selector.JerseySelectorDialogBuilder
import com.kuba.myeleven.utils.CustomBundler
import com.kuba.myeleven.utils.OperationError
import com.kuba.myeleven.utils.OperationStatus
import com.kuba.myeleven.utils.OperationSuccess
import icepick.State
import kotlinx.android.synthetic.main.activity_team_creation.*
import kotlinx.android.synthetic.main.widget_player.view.*

class TeamCreationActivity : BaseActivity<TeamCreationViewModel, ActivityTeamCreationBinding>(TeamCreationViewModel::class.java) {

    override val layoutId: Int
        get() = R.layout.activity_team_creation

    override val screenTitle: String
        get() = getString(R.string.team_creation_title)

    @InjectExtra
    @JvmField
    @State(CustomBundler::class)
    var teamId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = model
        initListeners()
        initObservers()
        if (teamId != null) {
            model.getAndBindTeam(teamId!!)
        }
    }

    private fun initObservers() {
        model.saveStatus.observe(this, Observer { onSaveStatusChanged(it) })
        model.firstJersey.observe(this, Observer { teamCreationFirstJerseyView.setAndBindJersey(it) })
        model.secondJersey.observe(this, Observer { teamCreationSecondJerseyView.setAndBindJersey(it) })
        model.thirdJersey.observe(this, Observer { teamCreationThirdJerseyView.setAndBindJersey(it) })
    }

    private fun showJerseySelector(model: MutableLiveData<Jersey>, title: String) {
        val dialog = JerseySelectorDialogBuilder(title).build()
        dialog.setListener(object: JerseySelectorDialog.JerseySelectorListener{
            override fun onSelect(jersey: Jersey) {
                model.value = jersey
            }
        })
        showDialogFragment(JerseySelectorDialog.TAG, dialog)
    }

    private fun onSaveStatusChanged(status: OperationStatus?) {
        when (status) {
            is OperationSuccess -> {
                navigator.finish(result = RESULT_OK, intent = intent)
            }
            is OperationError -> {
                applicationContext.toast("OperationError: ${status.throwable.message}")
            }
        }
    }

    private fun initListeners() {
        teamCreationSaveBtn.setOnClickListener {
            try {
                checkConditions()
                model.save(teamId != null)
            } catch (e: Exception) {
                showNativeMessage(title = getString(R.string.common_error_occured),
                        message = e.message!!,
                        confirmButtonText = getString(R.string.common_ok),
                        dialogCancelable = true,
                        drawableResId = R.drawable.ic_soccer_error)
            }
        }
        teamCreationFirstJersey.setOnClickListener { showJerseySelector(model.firstJersey, getString(R.string.team_creation_first_jersey)) }
        teamCreationSecondJersey.setOnClickListener { showJerseySelector(model.secondJersey, getString(R.string.team_creation_second_jersey)) }
        teamCreationThirdJerseyView.playerRoot.setOnClickListener { showJerseySelector(model.thirdJersey, getString(R.string.team_creation_third_jersey)) }
        teamCreationThirdJerseyChecker.setOnClickListener {
            model.onThirdJerseyCheckBoxClick()
        }
        teamCreationThirdJerseyCheckBox.setOnClickListener {
            model.onThirdJerseyCheckBoxClick()
        }
    }

    private fun checkConditions() {
        if (model.name.value?.isNotEmpty() != true) {
            throw Exception(getString(R.string.error_team_name))
        }
        if (model.firstJersey.value == model.secondJersey.value ||
                model.thirdJerseyEnable.value == true && (model.firstJersey.value == model.thirdJersey.value || model.secondJersey.value == model.thirdJersey.value)) {
            throw Exception(getString(R.string.error_team_same_jerseys))
        }
    }
}
