package com.kuba.myeleven.ui.splash

import android.animation.ValueAnimator
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import com.kuba.myeleven.R
import com.kuba.myeleven.databinding.ActivitySplashBinding
import com.kuba.myeleven.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_splash.*
import kotlin.math.pow

class SplashActivity : BaseActivity<SplashViewModel, ActivitySplashBinding>(SplashViewModel::class.java) {
    override val screenTitle: String
        get() = ""
    override val layoutId: Int
        get() = R.layout.activity_splash

    private val horizontalFrom = 0.15f
    private val horizontalMid = 0.40f
    private val horizontalTo = 0.79f
    private val verticalFrom = 0.70f
    private val verticalTo = 0.36f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        splashBall.startAnimation(AnimationUtils.loadAnimation(this, R.anim.infinite_rotation))
        val firstAnimation = ValueAnimator
                .ofFloat(horizontalFrom, horizontalTo)
                .setDuration(SCREEN_DELAY)
        firstAnimation.interpolator = DecelerateInterpolator()
        firstAnimation.addUpdateListener {
            ballGuidelineRight.setGuidelinePercent(horizontalFrom + (horizontalMid - horizontalFrom) * it.animatedValue as Float)
            ballGuidelineRight
                    .invalidate()
        }
        firstAnimation.start()
        Handler()
                .postDelayed({
                    val secondAnimation = ValueAnimator
                            .ofFloat(0f, 1f)
                            .setDuration(SCREEN_DELAY)
                    secondAnimation.interpolator = DecelerateInterpolator()
                    secondAnimation.addUpdateListener {
                        ballGuidelineRight.setGuidelinePercent(horizontalMid + (horizontalTo - horizontalMid) * ((it.animatedValue as Float - 1f).pow(3) + 1))
                        ballGuidelineTop.setGuidelinePercent(verticalFrom - (verticalFrom - verticalTo) * (it.animatedValue as Float).pow(3))
                        splashBall.scaleY = 1f - (0.5f * (it.animatedValue as Float).pow(3))
                        splashBall.scaleX = 1f - (0.5f * (it.animatedValue as Float).pow(3))
                    }
                    secondAnimation.start()
                }, SCREEN_DELAY)
        Handler()
                .postDelayed({
                    navigator.goToHomeActivity(getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE).getBoolean("showWelcomeDialog", false))
                    navigator.finish()
                }, SCREEN_DELAY * 2
                )
    }

    companion object {
        const val SCREEN_DELAY = 1000L
    }
}