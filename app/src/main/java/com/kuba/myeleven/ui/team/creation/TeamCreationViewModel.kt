package com.kuba.myeleven.ui.team.creation

import androidx.lifecycle.MutableLiveData
import com.kuba.myeleven.data.entities.Team
import com.kuba.myeleven.data.enums.Jersey
import com.kuba.myeleven.repository.TeamsRepository
import com.kuba.myeleven.ui.base.BaseViewModel
import com.kuba.myeleven.ui.base.Factory
import com.kuba.myeleven.utils.OperationError
import com.kuba.myeleven.utils.OperationStatus
import com.kuba.myeleven.utils.OperationSuccess
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class TeamCreationViewModel : BaseViewModel() {

    @Inject
    lateinit var teamsRepository: TeamsRepository

    var team: Team? = null
    var teamId: Int = 0
    var saveStatus = MutableLiveData<OperationStatus>()
    var name = MutableLiveData<String>()
    var city = MutableLiveData<String>()
    var firstJersey = MutableLiveData<Jersey>().also { it.value = Jersey.values()[Random().nextInt(Jersey.values().size)] }
    var secondJersey = MutableLiveData<Jersey>().also { it.value = Jersey.values()[Random().nextInt(Jersey.values().size)] }
    var thirdJersey = MutableLiveData<Jersey>().also { it.value = Jersey.values()[Random().nextInt(Jersey.values().size)] }
    var thirdJerseyEnable = MutableLiveData<Boolean>()

    override fun init() {
        initRequest(SAVE_TEAM_REQUEST_ID,
                object : Factory<Observable<Unit>> {
                    override fun create(): Observable<Unit> {
                        return teamsRepository.saveTeam(team!!)
                                .subscribeOn(mainExecutionThread.scheduler())
                                .observeOn(postExecutionThread.scheduler)
                    }

                },
                {
                    stop(SAVE_TEAM_REQUEST_ID)
                    saveStatus.value = OperationSuccess
                },
                {
                    stop(SAVE_TEAM_REQUEST_ID)
                    saveStatus.value = OperationError(it)
                })
        initRequest(UPDATE_TEAM_REQUEST_ID,
                object : Factory<Observable<Unit>> {
                    override fun create(): Observable<Unit> {
                        return teamsRepository.updateTeam(team!!)
                                .subscribeOn(mainExecutionThread.scheduler())
                                .observeOn(postExecutionThread.scheduler)
                    }
                },
                {
                    stop(UPDATE_TEAM_REQUEST_ID)
                    saveStatus.value = OperationSuccess
                },
                {
                    stop(UPDATE_TEAM_REQUEST_ID)
                    saveStatus.value = OperationError(it)
                })
        initRequest(GET_TEAM_REQUEST_ID,
                object : Factory<Observable<Team>> {
                    override fun create(): Observable<Team> {
                        return teamsRepository.getTeam(teamId)
                                .subscribeOn(mainExecutionThread.scheduler())
                                .observeOn(postExecutionThread.scheduler)
                    }

                },
                {
                    stop(GET_TEAM_REQUEST_ID)
                    firstJersey.value = it.firstJersey
                    secondJersey.value = it.secondJersey
                    if (it.thirdJersey != null) {
                        thirdJerseyEnable.value = true
                        thirdJersey.value = it.thirdJersey
                    } else {
                        thirdJersey.value = Jersey.values()[Random().nextInt(Jersey.values().size)]
                    }
                    name.value = it.name
                    city.value = it.city
                },
                {
                    stop(GET_TEAM_REQUEST_ID)
                })
    }

    fun save(withUpdate: Boolean) {
        team = Team(name = name.value ?: "",
                city = city.value,
                firstJersey = firstJersey.value!!,
                secondJersey = secondJersey.value!!,
                thirdJersey = if (thirdJerseyEnable.value == true) thirdJersey.value else null)
        if (team?.id != null) {
            team!!.id = teamId
        }
        if (withUpdate) {
            start(UPDATE_TEAM_REQUEST_ID)
        } else {
            start(SAVE_TEAM_REQUEST_ID)
        }
    }

    fun onThirdJerseyCheckBoxClick() {
        thirdJerseyEnable.value = thirdJerseyEnable.value != true
    }

    fun getAndBindTeam(teamId: Int) {
        this.teamId = teamId
        start(GET_TEAM_REQUEST_ID)
    }


    companion object {
        private const val SAVE_TEAM_REQUEST_ID = 2000
        private const val UPDATE_TEAM_REQUEST_ID = 2001
        private const val GET_TEAM_REQUEST_ID = 2002
    }
}