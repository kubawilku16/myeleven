package com.kuba.myeleven.ui.team.manage.matches

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class TeamMatchesModule {

    @Binds
    abstract fun fragment(teamMatchesFragment: TeamMatchesFragment): Fragment
}