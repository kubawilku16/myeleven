package com.kuba.myeleven.ui.team.manage.players

import androidx.lifecycle.MutableLiveData
import com.kuba.myeleven.data.entities.Player
import com.kuba.myeleven.repository.PlayersRepository
import com.kuba.myeleven.ui.base.BaseViewModel
import com.kuba.myeleven.ui.base.Factory
import io.reactivex.Observable
import javax.inject.Inject

class TeamPlayersViewModel : BaseViewModel() {

    @Inject
    lateinit var playersRepository: PlayersRepository

    private var teamId = -1

    val players = MutableLiveData<MutableList<Player>>()

    override fun init() {
        initRequest(GET_PLAYERS_REQUEST_ID, object : Factory<Observable<List<Player>>> {
            override fun create(): Observable<List<Player>> {
                return playersRepository.getPlayers(teamId)
                        .subscribeOn(mainExecutionThread.scheduler())
                        .observeOn(postExecutionThread.scheduler)
            }
        },
                {
                    stop(GET_PLAYERS_REQUEST_ID)
                    players.value = it.toMutableList()
                },
                {
                    stop(GET_PLAYERS_REQUEST_ID)
                })
    }

    fun loadPlayers(teamId: Int) {
        this.teamId = teamId
        start(GET_PLAYERS_REQUEST_ID)
    }

    companion object {
        private const val GET_PLAYERS_REQUEST_ID = 3000
    }
}