package com.kuba.myeleven.ui.home.teams

import androidx.lifecycle.MutableLiveData
import com.kuba.myeleven.data.entities.Team
import com.kuba.myeleven.repository.TeamsRepository
import com.kuba.myeleven.ui.base.BaseViewModel
import com.kuba.myeleven.ui.base.Factory
import io.reactivex.Observable
import javax.inject.Inject

class TeamsViewModel : BaseViewModel() {

    @Inject
    lateinit var teamsRepository: TeamsRepository

    val teams = MutableLiveData<MutableList<Team>>()

    var view: TeamsView? = null
    var teamId: Int = -1

    override fun init() {
        initRequest(GET_TEAMS_REQUEST_ID, object : Factory<Observable<List<Team>>> {
            override fun create(): Observable<List<Team>> {
                return teamsRepository.getAllTeams()
                        .subscribeOn(mainExecutionThread.scheduler())
                        .observeOn(postExecutionThread.scheduler)
            }
        },
                {
                    stop(GET_TEAMS_REQUEST_ID)
                    teams.value?.addAll(it)
                    view?.onLoadSuccess(it)
                },
                {
                    stop(GET_TEAMS_REQUEST_ID)
                    view?.onLoadError(it)
                })
        initRequest(DELETE_TEAM_REQUEST_ID, object : Factory<Observable<Unit>> {
            override fun create(): Observable<Unit> {
                return teamsRepository.deleteTeam(teamId)
                        .subscribeOn(mainExecutionThread.scheduler())
                        .observeOn(postExecutionThread.scheduler)
            }
        },
                {
                    stop(DELETE_TEAM_REQUEST_ID)
                    view?.onDeleteSuccess()
                },
                {
                    stop(DELETE_TEAM_REQUEST_ID)
                    view?.onDeleteError(it)
                })
    }

    fun loadTeams() {
        start(GET_TEAMS_REQUEST_ID)
    }

    fun delete(teamId: Int) {
        this.teamId = teamId
        start(DELETE_TEAM_REQUEST_ID)
    }

    companion object {
        private const val GET_TEAMS_REQUEST_ID = 1000
        private const val DELETE_TEAM_REQUEST_ID = 1001
    }

    interface TeamsView {
        fun onLoadSuccess(result: List<Team>)
        fun onLoadError(throwable: Throwable)
        fun onDeleteSuccess()
        fun onDeleteError(throwable: Throwable)
    }
}