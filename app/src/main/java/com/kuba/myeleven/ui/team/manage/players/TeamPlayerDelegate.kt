package com.kuba.myeleven.ui.team.manage.players

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.kuba.myeleven.R
import com.kuba.myeleven.data.entities.Player
import com.kuba.myeleven.ui.views.list.BaseAdapterDelegate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_player.*

class TeamPlayerDelegate(private val listener: OnClickListener, val colorRes: Int) : BaseAdapterDelegate<Player, TeamPlayerDelegate.ViewHolder>(Player::class.java) {
    override val layoutId: Int
        get() = R.layout.list_item_player

    override fun createViewHolder(view: View): ViewHolder = ViewHolder(view)

    override fun bindViewHolder(item: Player, holder: ViewHolder, position: Int, itemsSize: Int) {
        holder.bind(item, position)
    }

    inner class ViewHolder(override var containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        @SuppressLint("SetTextI18n")
        fun bind(item: Player, position: Int) {
            with(item) {
                listPlayerImage.background.setColorFilter(ContextCompat.getColor(containerView.context, colorRes), PorterDuff.Mode.SRC_IN)
                listPlayerName.text = "${firstName[0]}. $lastName"
                listPlayerNumber.text = number.toString()
                itemView.setOnClickListener { listener.onClick(id, position, itemView) }
                itemView.transitionName = "${firstName[0]}. $lastName"
            }
        }
    }

    interface OnClickListener {
        fun onClick(playerId: Int, position: Int, itemView: View)
    }
}