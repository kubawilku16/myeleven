package com.kuba.myeleven.ui.team.manage.players

import android.app.Activity
import android.content.Intent
import android.graphics.Rect
import android.transition.*
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import com.kuba.myeleven.R
import com.kuba.myeleven.databinding.FragmentTeamPlayersBinding
import com.kuba.myeleven.ui.base.BaseFragment
import com.kuba.myeleven.ui.base.navigation.Navigator
import com.kuba.myeleven.ui.player.details.PlayerDetailsFragmentBuilder
import com.kuba.myeleven.ui.team.manage.TeamManageActivity
import com.kuba.myeleven.ui.views.inbox.SlideExplode
import com.kuba.myeleven.ui.views.inbox.setCommonInterpolator
import com.kuba.myeleven.ui.views.list.BaseDelegationListAdapter
import com.kuba.myeleven.utils.RecyclerScrollFabListener
import icepick.State
import kotlinx.android.synthetic.main.fragment_team_players.*

@FragmentWithArgs
class TeamPlayersFragment : BaseFragment<TeamPlayersViewModel, FragmentTeamPlayersBinding>(TeamPlayersViewModel::class.java) {
    override val layoutId: Int
        get() = R.layout.fragment_team_players

    override val screenTitle: String
        get() = ""

    @Arg
    var teamId: Int = -1

    @Arg
    var mainColor: Int = -1

    @Arg
    @State
    @JvmField
    var teamName: String = ""

    @State
    @JvmField
    var tapPosition = NO_POSITION

    private val viewRect = Rect()
    private val transitionInterpolator = FastOutSlowInInterpolator()
    private val TRANSITION_DURATION = 1000L

    private lateinit var adapter: BaseDelegationListAdapter

    override fun initView() {
        if (activity is TeamManageActivity) {
            (activity as TeamManageActivity).prepareToolbar(teamName)
        }
        initRecycler()
        postponeEnterTransition()
        initObserver()
        teamPlayersFab.setOnClickListener { navigator.goToPlayerCreationActivity(teamId = teamId, playerId = null) }
        if (model.players.value == null) {
            model.loadPlayers(teamId)
        }
    }

    private fun initObserver() {
        model.players.observe(this, Observer { it ->
            startPostponedEnterTransition()
            adapter.setReloadedList(it)
            (view?.parent as? ViewGroup)?.doOnPreDraw {
                if (exitTransition == null) {
                    exitTransition = SlideExplode().apply {
                        duration = TRANSITION_DURATION
                        interpolator = transitionInterpolator
                    }
                }
                (teamPlayersList.layoutManager as LinearLayoutManager).findViewByPosition(tapPosition)?.let { view ->
                    view.getGlobalVisibleRect(viewRect)
                    (exitTransition as Transition).epicenterCallback =
                            object : Transition.EpicenterCallback() {
                                override fun onGetEpicenter(transition: Transition) = viewRect
                            }
                }

                startPostponedEnterTransition()
            }
        })
    }

    private fun initRecycler() {
        adapter = BaseDelegationListAdapter()
        adapter.addDelegate(TeamPlayerDelegate(object : TeamPlayerDelegate.OnClickListener {
            override fun onClick(playerId: Int, position: Int, itemView: View) {
                tapPosition = position
                itemView.getGlobalVisibleRect(viewRect)

                (this@TeamPlayersFragment.exitTransition as Transition).epicenterCallback =
                        object : Transition.EpicenterCallback() {
                            override fun onGetEpicenter(transition: Transition) = viewRect
                        }
                val sharedElementTransition = TransitionSet()
                        .addTransition(ChangeBounds())
                        .addTransition(ChangeTransform())
                        .addTransition(ChangeImageTransform()).apply {
                            duration = TRANSITION_DURATION
                            setCommonInterpolator(transitionInterpolator)
                        }
                val fragment = PlayerDetailsFragmentBuilder(playerId).build().apply {
                    sharedElementEnterTransition = sharedElementTransition
                    sharedElementReturnTransition = sharedElementTransition
                }
                activity!!.supportFragmentManager
                        .beginTransaction()
                        .setReorderingAllowed(true)
                        .replace(R.id.teamManageContainer, fragment)
                        .addToBackStack(null)
                        .addSharedElement(itemView, getString(R.string.transition_name))
                        .commit()
                        .also {
                            if (activity is TeamManageActivity) {
                                (activity as TeamManageActivity).hideNavigation()
                            }
                        }
            }
        }, mainColor))
        teamPlayersList.setEmptyView(teamPlayersPlaceholder)
        teamPlayersList.adapter = adapter
        teamPlayersList.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        teamPlayersList.addOnScrollListener(RecyclerScrollFabListener(teamPlayersFab))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Navigator.SAVE_PLAYER_SUCCESS_REQUEST_CODE) {
            model.loadPlayers(teamId)
        }
    }
}
