package com.kuba.myeleven.ui.base

import android.os.Handler

class DialogStateManager {
    private val dialogHandler = Handler()

    fun addRunnable(runnable: Runnable, time: Long = 3000L) {
        dialogHandler.postDelayed(runnable, time)
    }

    fun removeRunnable(runnable: Runnable? = null) {
        if (runnable == null)
            dialogHandler.removeCallbacksAndMessages(null)
        else
            dialogHandler.removeCallbacks(runnable)
    }
}