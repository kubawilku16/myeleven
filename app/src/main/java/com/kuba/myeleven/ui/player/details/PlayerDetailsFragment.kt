package com.kuba.myeleven.ui.player.details

import android.animation.ObjectAnimator
import android.view.View
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.kuba.myeleven.R
import com.kuba.myeleven.databinding.FragmentPlayerDetailsBinding
import com.kuba.myeleven.ext.toast
import com.kuba.myeleven.ui.base.BaseFragment
import com.kuba.myeleven.ui.team.manage.TeamManageActivity
import kotlinx.android.synthetic.main.fragment_player_details.*

class PlayerDetailsFragment : BaseFragment<PlayerDetailsViewModel, FragmentPlayerDetailsBinding>(PlayerDetailsViewModel::class.java) {
    override val layoutId: Int
        get() = R.layout.fragment_player_details
    override val screenTitle: String
        get() = ""

    @Arg
    var playerId: Int = 0

    override fun initView() {
        initToolbar()
        ObjectAnimator.ofFloat(playerDetailsContent, View.ALPHA, 0f, 1f).apply {
            startDelay = 50
            duration = 150
            start()
        }
    }

    private fun initToolbar() {
        if (activity is TeamManageActivity) {
            (activity as TeamManageActivity).prepareToolbar(
                    deleteListener = { context?.toast("Delete") },
                    editListener = { context?.toast("Edit") }
            )
        }
    }

}