package com.kuba.myeleven.ui.home.teams

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import androidx.core.content.ContextCompat
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kuba.myeleven.R
import com.kuba.myeleven.data.entities.Team
import com.kuba.myeleven.ui.views.list.BaseAdapterDelegate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_team.*

class TeamDelegate(val clickListener: OnClickListener) : BaseAdapterDelegate<Team, TeamDelegate.ViewHolder>(Team::class.java) {
    override val layoutId: Int
        get() = R.layout.list_item_team

    override fun createViewHolder(view: View): ViewHolder = ViewHolder(view)

    override fun bindViewHolder(item: Team, holder: ViewHolder, position: Int, itemsSize: Int) {
        holder.bind(item)
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        @SuppressLint("ResourceType")
        fun bind(item: Team) {
            with(item) {
                listTeamName.text = this.name
                val mainColor = if (firstJersey.mainColor != R.color.White) firstJersey.mainColor else firstJersey.numberColor
                if (firstJersey.mainColor != R.color.White) {
                    listTeamLogo.background.setColorFilter(ContextCompat.getColor(containerView.context, mainColor), PorterDuff.Mode.SRC_IN)
                } else {
                    listTeamLogo.background.setColorFilter(ContextCompat.getColor(containerView.context, mainColor), PorterDuff.Mode.SRC_IN)
                }
                containerView.setOnClickListener { clickListener.onClick(this.id, name, mainColor) }
                containerView.setOnLongClickListener {
                    clickListener.onLonClick(this.id, name)
                    true
                }
            }
        }
    }

    interface OnClickListener {
        fun onClick(id: Int, name: String, colorRes: Int)
        fun onLonClick(id: Int, name: String)
    }
}