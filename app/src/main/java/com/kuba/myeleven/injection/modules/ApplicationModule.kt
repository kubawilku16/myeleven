package com.kuba.myeleven.injection.modules

import android.content.Context
import com.kuba.myeleven.App
import com.kuba.myeleven.executors.MainExecutionThread
import com.kuba.myeleven.executors.PostExecutionThread
import com.kuba.myeleven.executors.ThreadExecutor
import com.kuba.myeleven.executors.impl.ExecutionThread
import com.kuba.myeleven.executors.impl.TaskExecutor
import com.kuba.myeleven.executors.impl.UiThread
import com.kuba.myeleven.ui.base.DialogStateManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class ApplicationModule {
    @Provides
    @Singleton
    fun provideContext(app: App): Context {
        return app
    }

    @Provides
    @Singleton
    fun provideDialogStateManager() = DialogStateManager()

    @Provides
    @Singleton
    fun provideThreadExecutor(taskExecutor: TaskExecutor): ThreadExecutor {
        return taskExecutor
    }

    @Provides
    @Singleton
    fun provideMainExecutionThread(threadExecutor: ThreadExecutor): MainExecutionThread {
        return ExecutionThread(threadExecutor)
    }

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }

}