package com.kuba.myeleven.injection.modules

import android.content.Context
import androidx.room.Room
import com.kuba.myeleven.R
import com.kuba.myeleven.data.AppDatabase
import com.kuba.myeleven.data.dao.EventDao
import com.kuba.myeleven.data.dao.MatchLineUpsDao
import com.kuba.myeleven.data.dao.PlayersDao
import com.kuba.myeleven.data.dao.TeamsDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseBindingModule {
    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    context.getString(R.string.app_name)).allowMainThreadQueries().build()

    @Provides
    @Singleton
    fun provideEventDao(appDatabase: AppDatabase): EventDao = appDatabase.eventsDao()

    @Provides
    @Singleton
    fun provideMatchLineUpDao(appDatabase: AppDatabase): MatchLineUpsDao = appDatabase.matchLineUpsDao()

    @Provides
    @Singleton
    fun providePlayersDao(appDatabase: AppDatabase): PlayersDao = appDatabase.playersDao()

    @Provides
    @Singleton
    fun provideTeamsDao(appDatabase: AppDatabase): TeamsDao = appDatabase.teamsDao()
}