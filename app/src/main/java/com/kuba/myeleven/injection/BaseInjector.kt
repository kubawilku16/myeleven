package com.kuba.myeleven.injection

interface BaseInjector {
    fun inject(target: Any)
}