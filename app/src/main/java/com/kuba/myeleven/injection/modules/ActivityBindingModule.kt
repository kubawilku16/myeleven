package com.kuba.myeleven.injection.modules

import com.kuba.myeleven.ui.home.HomeActivity
import com.kuba.myeleven.ui.home.HomeModule
import com.kuba.myeleven.ui.match.lineup.TeamLineupCreationActivity
import com.kuba.myeleven.ui.match.lineup.TeamLineupCreationModule
import com.kuba.myeleven.ui.player.creation.PlayerCreationActivity
import com.kuba.myeleven.ui.player.creation.PlayerCreationModule
import com.kuba.myeleven.ui.splash.SplashActivity
import com.kuba.myeleven.ui.splash.SplashModule
import com.kuba.myeleven.ui.team.creation.TeamCreationActivity
import com.kuba.myeleven.ui.team.creation.TeamCreationModule
import com.kuba.myeleven.ui.team.manage.TeamManageActivity
import com.kuba.myeleven.ui.team.manage.TeamManageModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {
    /**
     * Example to use:
     * @ContributesAndroidInjector(modules = [(ExampleModule::class)]
     * abstract fun bindExampleActivity(): ExampleActivity
     */
    
    @ContributesAndroidInjector(modules = [(SplashModule::class)])
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [(HomeModule::class)])
    abstract fun bindHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [(TeamCreationModule::class)])
    abstract fun bindTeamCreationActivity(): TeamCreationActivity

    @ContributesAndroidInjector(modules = [(TeamManageModule::class)])
    abstract fun bindTeamManageActivity(): TeamManageActivity

    @ContributesAndroidInjector(modules = [(PlayerCreationModule::class)])
    abstract fun bindPlayerCreationActivity(): PlayerCreationActivity

    @ContributesAndroidInjector(modules = [(TeamLineupCreationModule::class)])
    abstract fun bindMatchActivity(): TeamLineupCreationActivity
}