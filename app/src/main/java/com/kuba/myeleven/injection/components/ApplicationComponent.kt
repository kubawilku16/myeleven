package com.kuba.myeleven.injection.components

import com.kuba.myeleven.App
import com.kuba.myeleven.injection.modules.ActivityBindingModule
import com.kuba.myeleven.injection.modules.ApplicationModule
import com.kuba.myeleven.injection.modules.DatabaseBindingModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class),(ActivityBindingModule::class), (ApplicationModule::class), (DatabaseBindingModule::class)])
interface ApplicationComponent : AndroidInjector<App>, ViewModelComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): ApplicationComponent
    }
}