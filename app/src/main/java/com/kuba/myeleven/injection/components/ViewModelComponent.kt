package com.kuba.myeleven.injection.components

import com.kuba.myeleven.ui.home.HomeViewModel
import com.kuba.myeleven.ui.home.lienups.LineupsViewModel
import com.kuba.myeleven.ui.home.teams.TeamsViewModel
import com.kuba.myeleven.ui.match.lineup.TeamLineupCreationViewModel
import com.kuba.myeleven.ui.player.creation.PlayerCreationViewModel
import com.kuba.myeleven.ui.player.details.PlayerDetailsViewModel
import com.kuba.myeleven.ui.splash.SplashViewModel
import com.kuba.myeleven.ui.team.creation.TeamCreationViewModel
import com.kuba.myeleven.ui.team.manage.TeamManageViewModel
import com.kuba.myeleven.ui.team.manage.matches.TeamMatchesViewModel
import com.kuba.myeleven.ui.team.manage.players.TeamPlayersViewModel

interface ViewModelComponent {
    fun inject(viewModel: TeamCreationViewModel)
    fun inject(viewModel: LineupsViewModel)
    fun inject(viewModel: TeamsViewModel)
    fun inject(viewModel: SplashViewModel)
    fun inject(viewModel: HomeViewModel)
    fun inject(viewModel: TeamMatchesViewModel)
    fun inject(viewModel: TeamManageViewModel)
    fun inject(viewModel: TeamPlayersViewModel)
    fun inject(viewModel: PlayerCreationViewModel)
    fun inject(viewModel: PlayerDetailsViewModel)
    fun inject(viewModel: TeamLineupCreationViewModel)
}