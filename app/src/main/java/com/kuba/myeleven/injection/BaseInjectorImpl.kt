package com.kuba.myeleven.injection

import java.lang.reflect.Method
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class BaseInjectorImpl<T>(private val componentClass: Class<T>, private val component: T) : BaseInjector {

    private val methods: HashMap<Class<*>, Method>

    fun getComponent(): T {
        return component
    }

    override fun inject(target: Any) {

        var targetClass: Class<*>? = target.javaClass
        var method: Method? = methods[targetClass]
        while (method == null && targetClass != null) {
            targetClass = targetClass.superclass
            method = methods[targetClass]
        }

        if (method == null)
            throw RuntimeException(String.format("No %s injecting method exists in %s component", target.javaClass, componentClass))

        try {
            method.invoke(component, target)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

    private val cache = ConcurrentHashMap<Class<*>, HashMap<Class<*>, Method>>()

    private fun getMethods(componentClass: Class<*>): HashMap<Class<*>, Method> {
        var methods: HashMap<Class<*>, Method>? = cache[componentClass]
        if (methods == null) {
            synchronized(cache) {
                methods = cache[componentClass]
                if (methods == null) {
                    methods = HashMap()
                    for (method in componentClass.methods) {
                        val params = method.parameterTypes
                        if (params.size == 1)
                            methods!![params[0]] = method
                    }
                    cache[componentClass] = methods!!
                }
            }
        }
        return methods!!
    }

    init {
        this.methods = getMethods(componentClass)
    }
}