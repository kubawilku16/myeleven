package com.kuba.myeleven.executors

import io.reactivex.Scheduler

interface PostExecutionThread {
    val scheduler: Scheduler
}