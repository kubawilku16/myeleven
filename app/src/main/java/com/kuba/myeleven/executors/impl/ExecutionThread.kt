package com.kuba.myeleven.executors.impl

import com.kuba.myeleven.executors.MainExecutionThread
import com.kuba.myeleven.executors.ThreadExecutor
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Scheduler for testing
 */
class ExecutionThread @Inject internal constructor(private val threadExecutor: ThreadExecutor) : MainExecutionThread {

    override fun scheduler(): Scheduler {
        return Schedulers.from(threadExecutor)
    }
}