package com.kuba.myeleven.executors

import io.reactivex.Scheduler

interface MainExecutionThread {
    fun scheduler(): Scheduler
}