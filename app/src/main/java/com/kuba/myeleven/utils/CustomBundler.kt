package com.kuba.myeleven.utils

import android.os.Bundle
import icepick.Bundler
import org.parceler.Parcels

class CustomBundler : Bundler<Any> {
    override fun put(s: String, example: Any?, bundle: Bundle) {
        example?.let {
            bundle.putParcelable(s, Parcels.wrap(it))
        }
    }

    override fun get(s: String, bundle: Bundle): Any? {
        return Parcels.unwrap(bundle.getParcelable(s))
    }
}