package com.kuba.myeleven.utils

sealed class OperationStatus

object OperationSuccess : OperationStatus()
class OperationError(val throwable: Throwable) : OperationStatus()