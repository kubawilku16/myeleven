package com.kuba.myeleven.utils

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class RecyclerScrollFabListener(private val fab: FloatingActionButton) : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy > 0 && fab.visibility == View.VISIBLE) {
            fab.hide()
        } else if (dy < 0 && fab.visibility != View.VISIBLE) {
            fab.show()
        }
    }
}