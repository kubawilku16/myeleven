package com.kuba.myeleven.utils

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager

class KeyboardUtils {

    companion object {
        /**
         * Responsible for hiding keyboard for [view]
         * @param view
         */
        fun hideKeyboard(view: View?) {
            view?.let {

                val imm = it.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
                it.isFocusable = false
                it.isFocusableInTouchMode = false
                it.isFocusable = true
                it.isFocusableInTouchMode = true
                it.requestFocus()
            }
        }

        /**
         * Responsible for hiding keyboard for [activity]
         * @param activity
         */
        fun hideKeyboard(activity: Activity?) {
            activity?.let {
                hideKeyboard(it.currentFocus)
            }
        }

        /**
         * Responsible for hiding keyboard for [fragment]
         * @param fragment
         */
        fun hideKeyboard(fragment: Fragment) {
            hideKeyboard(fragment.view)
        }

        /**
         * Responsible for showing keyboard for [view]
         * @param view
         */
        fun showKeyboard(view: View) {
            val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, 0)
        }
    }


}