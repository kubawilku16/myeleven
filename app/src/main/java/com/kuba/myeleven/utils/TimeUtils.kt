package com.kuba.myeleven.utils

import java.text.SimpleDateFormat
import java.util.*

class TimeUtils {
    companion object {
        val DATE_PATTERN = "dd-MM-yyyy"
        val TIME_PATTERN = "dd-MM-yyyy HH:mm"

        fun convertDateToString(date: Date?): String {
            var result = ""
            try {
                result = SimpleDateFormat(DATE_PATTERN, Locale.getDefault()).format(date)
            } catch (e: Exception) {
                //Ignored
            }
            return result
        }

        fun convertToDate(date: String): Date? {
            return SimpleDateFormat(DATE_PATTERN, Locale.getDefault()).parse(date)
        }

        fun convertTimeToString(date: Date?): String {
            var result = ""
            try {
                result = SimpleDateFormat(TIME_PATTERN, Locale.getDefault()).format(date)
            } catch (e: Exception) {
                //Ignored
            }
            return result
        }
    }
}