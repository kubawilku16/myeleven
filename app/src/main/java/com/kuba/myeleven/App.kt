package com.kuba.myeleven

import android.app.Activity
import android.app.Application
import com.kuba.myeleven.injection.BaseInjector
import com.kuba.myeleven.injection.BaseInjectorImpl
import com.kuba.myeleven.injection.components.ApplicationComponent
import com.kuba.myeleven.injection.components.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

class App : Application(), HasActivityInjector, BaseInjector {
    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    private lateinit var appComponent: ApplicationComponent

    private lateinit var injector: BaseInjectorImpl<ApplicationComponent>

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerApplicationComponent
                .builder()
                .application(this)
                .build()
        injector = BaseInjectorImpl(ApplicationComponent::class.java, appComponent)
        appComponent.inject(this)
        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityDispatchingAndroidInjector

    override fun inject(target: Any) {
        injector.inject(target)
    }
}