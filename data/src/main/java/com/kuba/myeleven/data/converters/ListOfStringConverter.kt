package com.kuba.myeleven.data.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ListOfStringConverter {
    @TypeConverter
    fun toString(positions: List<String>) = Gson().toJson(positions)

    @TypeConverter
    fun toList(positions: String): List<String> =
            if (positions.isEmpty()) {
                emptyList()
            } else {
                Gson().fromJson(positions, object : TypeToken<List<String>>() {}.type)
            }
}