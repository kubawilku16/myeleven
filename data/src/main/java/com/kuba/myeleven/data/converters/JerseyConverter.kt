package com.kuba.myeleven.data.converters

import androidx.room.TypeConverter
import com.kuba.myeleven.data.enums.Jersey

class JerseyConverter {

    @TypeConverter
    fun toString(jersey: Jersey?) = jersey?.toString() ?: ""

    @TypeConverter
    fun toJersey(jersey: String) = if (jersey.isEmpty()) null else Jersey.valueOf(jersey)
}