package com.kuba.myeleven.data.converters

import androidx.room.TypeConverter
import com.kuba.myeleven.data.enums.LineUp

class LineUpConverter {
    @TypeConverter
    fun toString(lineUp: LineUp) = lineUp.toString()

    @TypeConverter
    fun toLineUp(lineUp: String) = LineUp.valueOf(lineUp)
}