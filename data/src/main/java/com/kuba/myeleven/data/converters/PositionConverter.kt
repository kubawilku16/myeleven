package com.kuba.myeleven.data.converters

import androidx.room.TypeConverter
import com.kuba.myeleven.data.enums.Position

class PositionConverter {
    @TypeConverter
    fun toString(positions: List<Position>) = positions.joinToString { it.value.toString() }

    @TypeConverter
    fun toList(positions: String) = positions.split(",").map {
        when(Integer.parseInt(it.trim())){
            0 -> Position.GOALKEEPER
            1 -> Position.DEFENDER
            2 -> Position.MIDFIELDER
            3 -> Position.STRIKER
            else ->  throw IllegalArgumentException("Wrong position number")
        }
    }
}