package com.kuba.myeleven.data.dao

import androidx.room.*
import com.kuba.myeleven.data.entities.MatchLineUp
import com.kuba.myeleven.data.entities.PlayerMatchStats
import java.util.*

@Dao
interface MatchLineUpsDao {
    @Query("SELECT * FROM MATCH_LINEUPS WHERE matchId = :matchId")
    fun getMatchLineUpDetailsById(matchId: Int): MatchLineUp

    @Query("SELECT * FROM PLAYER_STATS WHERE matchId = :matchId")
    fun getLineUpByMatchId(matchId: Int): List<PlayerMatchStats>

    @Query("SELECT * FROM MATCH_LINEUPS WHERE teamId = :teamId")
    fun getTeamLineUps(teamId: Int): List<MatchLineUp>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(playerMatchStats: PlayerMatchStats)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(matchLineUp: MatchLineUp)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(matchLineUp: MatchLineUp)

    @Delete
    fun delete(matchLineUp: MatchLineUp)

    @Delete
    fun delete(playerMatchStats: PlayerMatchStats)

    @Query("SELECT MAX(1) matchId FROM MATCH_LINEUPS WHERE editTime = :editTime")
    fun getLastLineupId(editTime: Date): Int

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(playersStats: MutableList<PlayerMatchStats>)
}