package com.kuba.myeleven.data.enums

import androidx.annotation.IdRes
import com.kuba.myeleven.data.R

enum class Position(val value: Int) {
    GOALKEEPER(0),
    DEFENDER(1),
    MIDFIELDER(2),
    STRIKER(3)
}

enum class EventType {
    GOAL,
    YELLOW_CARD,
    RED_CARD,
    CHANGE_IN,
    CHANGE_OUT,
    SECOND_YELLOW_CARD,
    OWN_GOAL
}

enum class BetterLeg {
    LEFT,
    RIGHT
}

enum class Jersey(@IdRes val mainColor: Int, @IdRes val numberColor: Int) {
    J1(R.color.White, R.color.Black),
    J2(R.color.Yellow, R.color.Black),
    J3(R.color.Yellow, R.color.White),
    J4(R.color.Gold, R.color.Black),
    J5(R.color.Gold, R.color.White),
    J6(R.color.Gold, R.color.MediumBlue),
    J7(R.color.Orange, R.color.Black),
    J8(R.color.Pink, R.color.Black),
    J9(R.color.Red, R.color.Black),
    J10(R.color.Red, R.color.White),
    J11(R.color.DarkRed, R.color.Black),
    J12(R.color.DarkRed, R.color.White),
    J13(R.color.Gray, R.color.White),
    J14(R.color.Gray, R.color.Yellow),
    J15(R.color.Steel, R.color.White),
    J16(R.color.Steel, R.color.Yellow),
    J17(R.color.Steel, R.color.DarkBlue),
    J18(R.color.Steel, R.color.Red),
    J19(R.color.Purple, R.color.White),
    J20(R.color.SteelBlue, R.color.White),
    J21(R.color.DodgerBlue, R.color.White),
    J22(R.color.DodgerBlue, R.color.Red),
    J23(R.color.Cyan, R.color.Black),
    J24(R.color.Lime, R.color.White),
    J25(R.color.Lime, R.color.Black),
    J26(R.color.Lime, R.color.MediumBlue),
    J27(R.color.Green, R.color.White),
    J28(R.color.Green, R.color.Black),
    J29(R.color.Green, R.color.MediumBlue),
    J30(R.color.MediumBlue, R.color.White),
    J31(R.color.MediumBlue, R.color.Gray),
    J32(R.color.MediumBlue, R.color.Black),
    J33(R.color.DarkBlue, R.color.Red),
    J34(R.color.DarkBlue, R.color.White),
    J35(R.color.DarkBlue, R.color.Gray),
    J36(R.color.Black, R.color.Red),
    J37(R.color.Black, R.color.Yellow),
    J38(R.color.Black, R.color.White),
}

enum class LineUp(val def: Int, val mid: Int, val extraMid: Int, val str: Int) {
    L_3_5_2(3, 5, 0, 2),
    L_3_4_3(3, 4, 0, 3),
    L_5_3_2(5, 3, 0, 2),
    L_5_4_1(5, 4, 0, 1),
    L_5_2_3(5, 2, 0, 3),
    L_4_4_2(4, 4, 0, 2),
    L_4_5_1(4, 5, 0, 1),
    L_4_2_3_1(4, 2, 3, 1),
    L_4_2_1_3(4, 2, 1, 3),
    L_4_3_3(4, 3, 0, 3);

    fun getLineUpText(): String {
        return StringBuilder()
                .append(def)
                .append("-")
                .append(mid)
                .append("-")
                .append(if (extraMid > 0) "$extraMid-" else "")
                .append(str).toString()
    }
}