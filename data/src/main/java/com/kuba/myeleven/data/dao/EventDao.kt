package com.kuba.myeleven.data.dao

import androidx.room.*
import com.kuba.myeleven.data.entities.Event

@Dao
interface EventDao {
    @Query("SELECT * FROM Events WHERE playerId = :playerId")
    fun getPlayerEvents(playerId: Int): List<Event>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(event: Event)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(event: Event)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(events: List<Event>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(events: List<Event>)

    @Delete
    fun delete(event: Event)
}