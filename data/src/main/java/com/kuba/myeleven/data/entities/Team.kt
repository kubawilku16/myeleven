package com.kuba.myeleven.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.kuba.myeleven.data.enums.Jersey

@Entity(tableName = "Teams")
data class Team(
        @PrimaryKey(autoGenerate = true)
        var id: Int = 0,
        var name: String,
        var firstJersey: Jersey,
        var secondJersey: Jersey,
        var thirdJersey: Jersey?,
        var city: String?
)