package com.kuba.myeleven.data.converters

import androidx.room.TypeConverter
import com.kuba.myeleven.data.enums.EventType

class EventConverter {
    @TypeConverter
    fun toInteger(eventType: EventType) = eventType.toString()

    @TypeConverter
    fun toEvent(event: String) = EventType.valueOf(event)
}