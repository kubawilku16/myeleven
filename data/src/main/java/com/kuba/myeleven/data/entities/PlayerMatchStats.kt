package com.kuba.myeleven.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.TypeConverters
import com.kuba.myeleven.data.converters.ListOfStringConverter

@Entity(tableName = "player_stats",
        primaryKeys = ["playerId", "matchId"],
        foreignKeys = [
            ForeignKey(
                    entity = Player::class,
                    onDelete = CASCADE,
                    childColumns = arrayOf("playerId"),
                    parentColumns = arrayOf("id")
            ),
            ForeignKey(
                    entity = MatchLineUp::class,
                    onDelete = CASCADE,
                    childColumns = arrayOf("matchId"),
                    parentColumns = arrayOf("matchId")
            )
        ]
)
data class PlayerMatchStats(
        val playerId: Int,
        var matchId: Int,
        var position: Int,
        @TypeConverters(ListOfStringConverter::class)
        var goals: List<String>?,
        var assists: Int?,
        var inOutMin: String?,
        var yellowCards: Int?,
        var redCard: Int?
)