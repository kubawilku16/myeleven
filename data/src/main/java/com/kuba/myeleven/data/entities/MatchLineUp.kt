package com.kuba.myeleven.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.kuba.myeleven.data.enums.Jersey
import com.kuba.myeleven.data.enums.LineUp
import java.util.*

@Entity(tableName = "match_lineups",
        foreignKeys =
        [ForeignKey(
                entity = Team::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("teamId"),
                onDelete = CASCADE
        )]
)
data class MatchLineUp(
        @PrimaryKey(autoGenerate = true)
        val matchId: Int,
        val teamId: Int,
        var lineup: LineUp,
        var jersey: Jersey,
        var editTime: Date = Date(System.currentTimeMillis()),
        var opponentName: String?,
        var time: Date?
)