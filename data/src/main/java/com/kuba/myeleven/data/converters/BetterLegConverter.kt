package com.kuba.myeleven.data.converters

import androidx.room.TypeConverter
import com.kuba.myeleven.data.enums.BetterLeg

class BetterLegConverter {
    @TypeConverter
    fun toString(betterLeg: BetterLeg) = betterLeg.toString()

    @TypeConverter
    fun toBetterLeg(betterLeg: String) = BetterLeg.valueOf(betterLeg)
}