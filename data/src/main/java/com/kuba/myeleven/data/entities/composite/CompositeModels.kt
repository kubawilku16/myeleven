package com.kuba.myeleven.data.entities.composite

import com.kuba.myeleven.data.entities.MatchLineUp
import com.kuba.myeleven.data.entities.Player
import com.kuba.myeleven.data.entities.PlayerMatchStats
import com.kuba.myeleven.data.entities.Team

data class TeamMatchModel(var team: Team, var lineup: MatchLineUp, var players: List<PlayerFullDetails>)

data class PlayerFullDetails(var player: Player, var stats: PlayerMatchStats)

data class TeamNewMatchModel(var team: Team, var players: List<Player>)
