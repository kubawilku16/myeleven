package com.kuba.myeleven.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kuba.myeleven.data.converters.*
import com.kuba.myeleven.data.dao.EventDao
import com.kuba.myeleven.data.dao.MatchLineUpsDao
import com.kuba.myeleven.data.dao.PlayersDao
import com.kuba.myeleven.data.dao.TeamsDao
import com.kuba.myeleven.data.entities.*

@Database(entities = [
    Event::class,
    MatchLineUp::class,
    Player::class,
    PlayerMatchStats::class,
    Team::class], version = 1, exportSchema = false
)
@TypeConverters(BetterLegConverter::class, DateConverter::class, EventConverter::class,
        PositionConverter::class, ListOfStringConverter::class, JerseyConverter::class,
        LineUpConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun eventsDao(): EventDao
    abstract fun matchLineUpsDao(): MatchLineUpsDao
    abstract fun playersDao(): PlayersDao
    abstract fun teamsDao(): TeamsDao
}