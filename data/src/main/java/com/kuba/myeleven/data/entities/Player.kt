package com.kuba.myeleven.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.kuba.myeleven.data.enums.BetterLeg
import com.kuba.myeleven.data.enums.Position
import java.util.*

@Entity(
        tableName = "players",
        foreignKeys = [
            ForeignKey(
                    entity = Team::class,
                    onDelete = CASCADE,
                    parentColumns = arrayOf("id"),
                    childColumns = arrayOf("teamId")
            )]
)
data class Player(
        @PrimaryKey(autoGenerate = true)
        var id: Int = 0,
        var firstName: String,
        var lastName: String,
        var number: Int,
        val teamId: Int,
        var positions: MutableList<Position>,
        var betterLeg: BetterLeg?,
        var birthDay: Date?
)