package com.kuba.myeleven.data.dao

import androidx.room.*
import com.kuba.myeleven.data.entities.Team

@Dao
interface TeamsDao {
    @Query("SELECT * FROM TEAMS")
    fun getTeams(): List<Team>

    @Query("SELECT * FROM TEAMS WHERE id=:id")
    fun getTeamById(id: Int): Team

    @Delete
    fun delete(team: Team)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(team: Team)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(team: Team)
}