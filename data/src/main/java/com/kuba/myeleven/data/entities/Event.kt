package com.kuba.myeleven.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.kuba.myeleven.data.converters.EventConverter
import com.kuba.myeleven.data.enums.EventType

@Entity(tableName = "events",
        foreignKeys = [
            ForeignKey(
                    entity = Player::class,
                    parentColumns = arrayOf("id"),
                    childColumns = arrayOf("playerId"),
                    onDelete = CASCADE),
            ForeignKey(
                    entity = MatchLineUp::class,
                    parentColumns = arrayOf("matchId"),
                    childColumns = arrayOf("matchId"),
                    onDelete = CASCADE
            )
        ])
data class Event(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        @TypeConverters(EventConverter::class)
        var eventType: EventType,
        val matchId: Int,
        var time: String,
        var playerId: Int
)