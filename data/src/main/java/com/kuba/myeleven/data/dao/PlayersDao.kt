package com.kuba.myeleven.data.dao

import androidx.room.*
import com.kuba.myeleven.data.entities.Player

@Dao
interface PlayersDao {

    @Query("SELECT * FROM PLAYERS where id = :playerId")
    fun getPlayerDetails(playerId: Int): Player

    @Query("SELECT * FROM PLAYERS where teamId = :teamId")
    fun getPlayersByTeamId(teamId: Int): List<Player>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(player: Player)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(player: Player)

    @Delete
    fun delete(player: Player)
}